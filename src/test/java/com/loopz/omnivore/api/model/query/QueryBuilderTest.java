package com.loopz.omnivore.api.model.query;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class QueryBuilderTest {


    @Test
    public void test1() {
        // https://api.omnivore.io/1.0/locations/cRyK7aAi/tickets?where=and(eq(open,true),eq(@employee.id,'24'))
        String whereClause = new QueryBuilder()
                .and()
                .eq(QueryField.OPEN, true)
                .eq(QueryField.EMPLOYEE_ID, "24")
                .build()
                .toString();

        assertEquals("where=and(eq(open,true),eq(@employee.id,'24'))", whereClause);
    }

    @Test
    public void test2() {
        // https://api.omnivore.io/1.0/locations/cRyK7aAi/tickets?where=and(eq(open,false),eq(@table.id,'24'))
        String whereClause = new QueryBuilder()
                .and()
                .eq(QueryField.OPEN, false)
                .eq(QueryField.TABLE_ID, "24")
                .build().toString();

        assertEquals("where=and(eq(open,false),eq(@table.id,'24'))", whereClause);
    }

    @Test
    public void test3() {
        // https://api.omnivore.io/1.0/locations?where=or(eq(health.healthy,false),eq(status,'offline'))
        String whereClause = new QueryBuilder()
                .or()
                .eq(QueryField.HEALTH_HEALTHY, false)
                .eq(QueryField.STATUS, "offline")
                .build().toString();

        assertEquals("where=or(eq(health.healthy,false),eq(status,'offline'))", whereClause);
    }

    @Test
    public void test4() {
        // https://api.omnivore.io/1.0/locations?where=eq(open,false)
        String whereClause = new QueryBuilder()
                .or()
                .eq(QueryField.OPEN, false)
                .build().toString();

        assertEquals("where=eq(open,false)", whereClause);
    }


}
