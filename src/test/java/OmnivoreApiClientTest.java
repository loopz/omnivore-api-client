import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreApiUrls;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.client.ApplicationKeyAuthenticationStrategy;
import com.loopz.omnivore.api.client.OmnivoreApiClient;
import com.loopz.omnivore.api.model.*;
import com.loopz.omnivore.api.model.list.LocationList;
import com.loopz.omnivore.api.model.list.MenuItemList;
import com.loopz.omnivore.api.model.list.TenderTypeList;
import com.loopz.omnivore.api.model.list.TicketItemList;
import com.loopz.omnivore.api.model.query.Query;
import com.loopz.omnivore.api.model.query.QueryBuilder;
import com.loopz.omnivore.api.model.query.QueryField;
import com.loopz.omnivore.api.model.request.AddTicketItemsRequest;
import com.loopz.omnivore.api.model.request.Make3rdPartyPaymentRequest;
import com.loopz.omnivore.api.model.request.OpenTicketRequest;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

public class OmnivoreApiClientTest {

    @BeforeClass
    public static void setupClass() {
        OmnivoreApi.setApiKey(TestApiKeys.V_1_0);
    }

    @Test
    public void testListLocations() throws OmnivoreException {
        Page<LocationList> page = LocationList.list();

        assertNotNull(page);
    }

    @Test
    public void testGetLocation() throws OmnivoreException {
        LocationList listResponse = LocationList.list().getList();
        Location location = Location.get(listResponse.getLocations().get(0).getId());

        assertNotNull(location);

        TicketList ticketList = location.getLinks().getTickets().getList();

        assertNotNull(ticketList);
    }

    @Test
    public void testListTickets() throws OmnivoreException {
        LocationList locations = LocationList.list().getList();
        Location location = locations.getLocations().get(0);

        TicketList tickets = TicketList.list(location.getId()).getList();

        assertNotNull(tickets);

    }

    @Test
    public void testListTicketsWithQeuery() throws OmnivoreException {
        LocationList locations = LocationList.list().getList();
        Location location = locations.getLocations().get(0);

        OmnivoreApiClient omnivoreApiClient = new OmnivoreApiClient(OmnivoreApiUrls.V_1_0, new ApplicationKeyAuthenticationStrategy(OmnivoreApi.getApiKey()));


        Query query = new QueryBuilder()
                .and()
                .eq(QueryField.TICKET_NUMBER, 215147)
                .eq(QueryField.OPEN, true)
                .build();

        Page<TicketList> tickets = omnivoreApiClient.listTickets(location.getId(), query);

        assertNotNull(tickets);

        for(int i = 0; i < tickets.getList().getTickets().size(); i++) {
            Ticket ticket = tickets.getList().getTickets().get(i);

            assertTrue(ticket.isOpen());
        }
    }

    @Test
    public void testListTicketItems() throws OmnivoreException {
        LocationList locations = LocationList.list().getList();
        Location location = locations.getLocations().get(0);

        TicketList tickets = TicketList.list(location.getId()).getList();

        Ticket ticket = tickets.getTickets().get(0);

        TicketItemList ticketItemList = TicketItemList.list(location.getId(), ticket.getId()).getList();

        assertNotNull(ticketItemList);
    }

    @Test
    public void testGetTicketItem() throws OmnivoreException {
        LocationList locations = LocationList.list().getList();
        Location location = locations.getLocations().get(0);

        TicketList tickets = TicketList.list(location.getId()).getList();

        Ticket ticket = Ticket.get(location.getId(), tickets.getTickets().get(0).getId());

        assertNotNull(ticket);
        assertNotNull(ticket.getPayments());
        assertNotNull(ticket.getDiscounts());
        assertNotNull(ticket.getEmployee());
        assertNotNull(ticket.getRevenueCenter());
        assertNotNull(ticket.getServiceCharges());
        assertNotNull(ticket.getItems());
        assertNotNull(ticket.getVoidedItems());
        assertNotNull(ticket.getLinks());

    }

    @Test
    public void testOpenAndVoidTicket() throws OmnivoreException {
        LocationList locations = LocationList.list().getList();
        Location location = locations.getLocations().get(0);

        Employee employee = location.getLinks().getEmployees().getList().getEmployees().get(0);
        RevenueCenter revenueCenter = location.getLinks().getRevenueCenters().getList().getRevenueCenters().get(0);
        OrderType orderType = location.getLinks().getOrderTypes().getList().getOrderTypes().get(0);
        Table table = location.getLinks().getTables().getList().getTables().get(0);

        OpenTicketRequest openTicketRequest = OpenTicketRequest.builder()
                .employeeId(employee.getId())
                .guestCount(8)
                .orderTypeId(orderType.getId())
                .revenueCenterId(revenueCenter.getId())
                .name("test ticket")
                .tableId(table.getId())
                .autoSend(false)
                .build();

        Ticket openedTicket = Ticket.open(location.getId(), openTicketRequest);

        assertThat(openedTicket.getTable().getId(), is(openTicketRequest.getTableId()));
        assertThat(openedTicket.getRevenueCenter().getId(), is(openTicketRequest.getRevenueCenterId()));
        assertThat(openedTicket.getOrderType().getId(), is(openTicketRequest.getOrderTypeId()));
        assertThat(openedTicket.getName(), is(openTicketRequest.getName()));
        assertThat(openedTicket.getGuestCount(), is(openTicketRequest.getGuestCount()));
        assertThat(openedTicket.isAutoSend(), is(openTicketRequest.isAutoSend()));
        assertThat(openedTicket.isOpen(), is(true));
        assertThat(openedTicket.isVoid(), is(false));

        Ticket voidedTicket = Ticket.cancel(location.getId(), openedTicket.getId(), true);

        assertThat(voidedTicket.getId(), is(openedTicket.getId()));
        assertThat(voidedTicket.isVoid(), is(true));
    }

    private TenderType get3rdPartyTenderType(TenderTypeList tenderTypeList) {
        for(TenderType tenderType : tenderTypeList.getTenderTypes()) {
            if(tenderType.getName().equals("3rd Party")) {
                return tenderType;
            }
        }
        throw new IllegalArgumentException("No 3rd Party tender type found in list");
    }

    @Test
    public void testMake3rdPartyPayment() throws OmnivoreException {
        LocationList locations = LocationList.list().getList();
        Location location = locations.getLocations().get(0);
        TenderType tenderType = get3rdPartyTenderType(location.getLinks().getTenderTypes().getList());

        Employee employee = location.getLinks().getEmployees().getList().getEmployees().get(0);
        RevenueCenter revenueCenter = location.getLinks().getRevenueCenters().getList().getRevenueCenters().get(0);
        OrderType orderType = location.getLinks().getOrderTypes().getList().getOrderTypes().get(0);
        Table table = location.getLinks().getTables().getList().getTables().get(0);

        OpenTicketRequest openTicketRequest = OpenTicketRequest.builder()
                .employeeId(employee.getId())
                .guestCount(8)
                .orderTypeId(orderType.getId())
                .revenueCenterId(revenueCenter.getId())
                .name("test ticket")
                .tableId(table.getId())
                .autoSend(false)
                .build();

        Ticket ticket = Ticket.open(location.getId(), openTicketRequest);

        assertThat(ticket.getPayments().isEmpty(), is(true));
        assertThat(ticket.isOpen(), is(true));
        assertThat(ticket.getItems().isEmpty(), is(true));

        // Add some items to the ticket
        MenuItemList menuItems = MenuItemList.list(location.getId()).getList();

        MenuItem menuItemOne = menuItems.getMenuItems().get(0);
        MenuItem menuItemTwo = menuItems.getMenuItems().get(1);

        AddTicketItemsRequest addTicketItemRequest = AddTicketItemsRequest.builder()
                .newTicketItem()
                .menuItemId(menuItemOne.getId())
                .quantity(2)
                .done()
                .newTicketItem()
                .menuItemId(menuItemTwo.getId())
                .quantity(4)
                .done()
                .build();

        ticket = Ticket.addItems(location.getId(), ticket.getId(), addTicketItemRequest);

        assertThat(ticket.getItems().size(), is(2));

        Make3rdPartyPaymentRequest paymentRequest = Make3rdPartyPaymentRequest.builder()
                .amount(ticket.getTotals().getDue())
//                .autoClose(true)
//                .comment("Store Credits Used")
                .tenderType(tenderType.getId())
                .build();

        Payment payment = Payment.make(location.getId(), ticket.getId(), paymentRequest);

        assertThat(payment.getAmount(), is(paymentRequest.getAmount()));
//        assertThat(payment.getType(), is(paymentRequest.getType()));

        Ticket paidTicket = Ticket.get(location.getId(), ticket.getId());

        assertThat(paidTicket.isOpen(), is(false));
        assertThat(paidTicket.getPayments().size(), is(1));
        Payment ticketPayment = paidTicket.getPayments().get(0);
        assertThat(ticketPayment.getId(), is(payment.getId()));
        assertThat(ticketPayment.getAmount(), is(payment.getAmount()));
    }

    @Test
    public void testGetTenderTypes() throws OmnivoreException {
        LocationList locations = LocationList.list().getList();
        Location location = locations.getLocations().get(0);

        Page<TenderTypeList> tenderTypes = location.getLinks().getTenderTypes();

        assertNotNull(tenderTypes);
    }

    @Test
    public void testAddTicketItem() throws OmnivoreException {
        LocationList locations = LocationList.list().getList();
        Location location = locations.getLocations().get(0);
        TenderType tenderType = get3rdPartyTenderType(location.getLinks().getTenderTypes().getList());

        Employee employee = location.getLinks().getEmployees().getList().getEmployees().get(0);
        RevenueCenter revenueCenter = location.getLinks().getRevenueCenters().getList().getRevenueCenters().get(0);
        OrderType orderType = location.getLinks().getOrderTypes().getList().getOrderTypes().get(0);
        Table table = location.getLinks().getTables().getList().getTables().get(0);
        MenuItemList menuItems = MenuItemList.list(location.getId()).getList();

        MenuItem menuItemOne = menuItems.getMenuItems().get(0);
        MenuItem menuItemTwo = menuItems.getMenuItems().get(1);

        OpenTicketRequest openTicketRequest = OpenTicketRequest.builder()
                .employeeId(employee.getId())
                .guestCount(8)
                .orderTypeId(orderType.getId())
                .revenueCenterId(revenueCenter.getId())
                .name("test ticket")
                .tableId(table.getId())
                .autoSend(false)
                .build();

        Ticket ticket = Ticket.open(location.getId(), openTicketRequest);

        assertThat(ticket.getItems().isEmpty(), is(true));

        AddTicketItemsRequest addTicketItemRequest = AddTicketItemsRequest.builder()
                .newTicketItem()
                .menuItemId(menuItemOne.getId())
                .quantity(2)
                .done()
                .newTicketItem()
                .menuItemId(menuItemTwo.getId())
                .quantity(4)
                .done()
                .build();

        ticket = Ticket.addItems(location.getId(), ticket.getId(), addTicketItemRequest);

        assertThat(ticket.getItems().size(), is(2));

    }
}
