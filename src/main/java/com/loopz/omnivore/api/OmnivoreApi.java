package com.loopz.omnivore.api;

import com.loopz.omnivore.api.client.OmnivoreApiClient;
import com.loopz.omnivore.api.client.ApplicationKeyAuthenticationStrategy;

/**
 * Holds the default configuration to use when interacting with the Omnivore API
 */
public class OmnivoreApi {

    private static String API_KEY = null;

    public static String getApiKey() {
        return API_KEY;
    }

    public static void setApiKey(String apiKey) {
         API_KEY = apiKey;
    }

    public static OmnivoreApiClient client() {
        return new OmnivoreApiClient(OmnivoreApiUrls.V_1_0, getAuthStrategy());
    }

    private static ApplicationKeyAuthenticationStrategy getAuthStrategy() {
        return new ApplicationKeyAuthenticationStrategy(OmnivoreApi.getApiKey());
    }

}
