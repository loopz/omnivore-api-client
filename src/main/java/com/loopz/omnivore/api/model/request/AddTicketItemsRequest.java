package com.loopz.omnivore.api.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddTicketItemsRequest {

    private final List<Map<String, Object>> ticketItems;

    private AddTicketItemsRequest(Builder builder) {
        ticketItems = builder.ticketItems;
    }

    public static Builder builder() {
        return new Builder();
    }

    @JsonProperty("items")
    public List<Map<String, Object>> getTicketItems() {
        return ticketItems;
    }

    public static final class Builder {

        private List<Map<String, Object>> ticketItems = new ArrayList<Map<String, Object>>();

        public NewTicketItemBuilder newTicketItem() {
            return new NewTicketItemBuilder();
        }

        public AddTicketItemsRequest build() {
            return new AddTicketItemsRequest(this);
        }

        public class NewTicketItemBuilder {

            private Map<String, Object> ticketItem = new HashMap<>();

            public NewTicketItemBuilder quantity(int quantity) {
                ticketItem.put("quantity", quantity);
                return this;
            }

            public NewTicketItemBuilder pricePerUnit(int pricePerUnit) {
                ticketItem.put("price_per_unit", pricePerUnit);
                return this;
            }

            public NewTicketItemBuilder priceLevelId(String priceLevelId) {
                ticketItem.put("price_level", priceLevelId);
                return this;
            }

            public NewTicketItemBuilder comment(String comment) {
                ticketItem.put("comment", comment);
                return this;
            }

            public NewTicketItemBuilder menuItemId(String menuItemId) {
                ticketItem.put("menu_item", menuItemId);
                return this;
            }

            public NewDiscountBuilder discount() {
                return new NewDiscountBuilder();
            }

            public Builder done() {
                ticketItems.add(ticketItem);
                return Builder.this;
            }

            public class NewDiscountBuilder {

                private Map<String, Object> discount = new HashMap<>();

                public NewDiscountBuilder discountId(String discountId) {
                    discount.put("discount", discountId);
                    return this;
                }

                public NewDiscountBuilder comment(String comment) {
                    discount.put("comment", comment);
                    return this;
                }

                public NewDiscountBuilder value(int value) {
                    discount.put("value", value);
                    return this;
                }

                public NewTicketItemBuilder done() {
                    if(!ticketItem.containsKey("discounts")) {
                        ticketItem.put("discounts", new ArrayList<>());
                    }
                    ArrayList<Object> discounts = (ArrayList<Object>) ticketItem.get("discounts");
                    discounts.add(discount);
                    return NewTicketItemBuilder.this;
                }

            }

        }

    }

}
