package com.loopz.omnivore.api.model.embedded;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.model.Table;
import java.util.List;

public class RevenueCenterEmbedded {

    private final List<Table> tables;

    public RevenueCenterEmbedded(@JsonProperty("tables") List<Table> tables) {
        this.tables = tables;
    }

    public List<Table> getTables() {
        return tables;
    }
}
