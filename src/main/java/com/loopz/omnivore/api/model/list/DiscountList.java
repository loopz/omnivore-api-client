package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.model.Discount;
import java.util.List;

public class DiscountList {

    private final List<Discount> discounts;

    public DiscountList(@JsonProperty("discounts") List<Discount> discounts) {
        this.discounts = discounts;
    }

    public List<Discount> getDiscounts() {
        return discounts;
    }
}
