package com.loopz.omnivore.api.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MakeCashPaymentRequest {

    private final String type = "cash";
    private final int tip;
    private final String tenderType;
    private final String comment;
    private final int amount;
    private final boolean autoClose;

    private MakeCashPaymentRequest(Builder builder) {
        tip = builder.tip;
        tenderType = builder.tenderType;
        comment = builder.comment;
        amount = builder.amount;
        autoClose = builder.autoClose;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getType() {
        return type;
    }

    public int getTip() {
        return tip;
    }

    @JsonProperty("tender_type")
    public String getTenderType() {
        return tenderType;
    }

    public String getComment() {
        return comment;
    }

    public int getAmount() {
        return amount;
    }

    @JsonProperty("auto_close")
    public boolean isAutoClose() {
        return autoClose;
    }

    public static final class Builder {
        private int tip;
        private String tenderType;
        private String comment;
        private int amount;
        private boolean autoClose;

        private Builder() {
        }

        public Builder tip(int tip) {
            this.tip = tip;
            return this;
        }

        public Builder tenderType(String tenderType) {
            this.tenderType = tenderType;
            return this;
        }

        public Builder comment(String comment) {
            this.comment = comment;
            return this;
        }

        public Builder amount(int amount) {
            this.amount = amount;
            return this;
        }

        public Builder autoClose(boolean autoClose) {
            this.autoClose = autoClose;
            return this;
        }

        public MakeCashPaymentRequest build() {
            return new MakeCashPaymentRequest(this);
        }
    }
}
