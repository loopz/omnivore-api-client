package com.loopz.omnivore.api.model.link;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Employee;
import com.loopz.omnivore.api.model.Job;

public class ClockEntryLinks {

    private final Link<Employee> employee;
    private final Link<Job> job;

    public ClockEntryLinks(
            @JsonProperty("employee") Link<Employee> employee,
            @JsonProperty("job") Link<Job> job) {
        this.employee = employee;
        this.job = job;
    }

    public Employee getEmployee() throws OmnivoreException {
        return OmnivoreApi.client().get(employee.getHref(), new TypeReference<Employee>() {});
    }

    public Job getJob() throws OmnivoreException {
        return OmnivoreApi.client().get(job.getHref(), new TypeReference<Job>() {});
    }
}
