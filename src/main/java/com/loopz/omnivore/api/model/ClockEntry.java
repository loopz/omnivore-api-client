package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.omnivore.api.model.embedded.ClockEntryEmbedded;
import com.loopz.omnivore.api.model.link.ClockEntryLinks;
import java.time.Instant;

@JsonDeserialize(builder = ClockEntry.Builder.class)
public class ClockEntry {

    private final String id;
    private final Instant clockIn;
    private final Instant clockOut;
    private final ClockEntryEmbedded embedded;
    private final ClockEntryLinks links;

    private ClockEntry(Builder builder) {
        id = builder.id;
        clockIn = builder.clockIn;
        clockOut = builder.clockOut;
        embedded = builder.embedded;
        links = builder.links;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public Instant getClockIn() {
        return clockIn;
    }

    public Instant getClockOut() {
        return clockOut;
    }

    public ClockEntryLinks getLinks() {
        return links;
    }

    public Employee getEmployee() {
        return embedded.getEmployee();
    }

    public Job getJob() {
        return embedded.getJob();
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private Instant clockIn;
        private Instant clockOut;
        private ClockEntryEmbedded embedded;
        private ClockEntryLinks links;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        @JsonProperty("clock_in")
        public Builder clockIn(Instant clockIn) {
            this.clockIn = clockIn;
            return this;
        }

        @JsonProperty("clock_out")
        public Builder clockOut(Instant clockOut) {
            this.clockOut = clockOut;
            return this;
        }

        @JsonProperty("_embedded")
        public Builder embedded(ClockEntryEmbedded embedded) {
            this.embedded = embedded;
            return this;
        }

        @JsonProperty("_links")
        public Builder links(ClockEntryLinks links) {
            this.links = links;
            return this;
        }

        public ClockEntry build() {
            return new ClockEntry(this);
        }
    }
}
