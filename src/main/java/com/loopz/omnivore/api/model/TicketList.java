package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.query.Query;
import com.loopz.omnivore.api.model.query.QueryBuilder;

import java.util.List;

public class TicketList {

    private final List<Ticket> tickets;

    public TicketList(@JsonProperty("tickets") List<Ticket> tickets) {
        this.tickets = tickets;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    public static Page<TicketList> list(String locationId) throws OmnivoreException {
        return OmnivoreApi.client().listTickets(locationId);
    }

    public static Page<TicketList> list(String locationId, Query query) throws OmnivoreException {
        return OmnivoreApi.client().listTickets(locationId, query);
    }

}
