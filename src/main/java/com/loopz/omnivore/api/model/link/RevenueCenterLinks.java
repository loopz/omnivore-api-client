package com.loopz.omnivore.api.model.link;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Page;
import com.loopz.omnivore.api.model.TicketList;
import com.loopz.omnivore.api.model.list.TableList;

public class RevenueCenterLinks {

    private final Link<Page<TicketList>> openTickets;
    private final Link<Page<TableList>> tables;

    public RevenueCenterLinks(
            @JsonProperty("open_tickets") Link<Page<TicketList>> openTickets,
            @JsonProperty("tables") Link<Page<TableList>> tables) {
        this.openTickets = openTickets;
        this.tables = tables;
    }

    public Page<TicketList> getOpenTickets() throws OmnivoreException {
        return OmnivoreApi.client().get(openTickets.getHref(), new TypeReference<Page<TicketList>>() {});
    }

    public Page<TableList> getTables() throws OmnivoreException {
        return OmnivoreApi.client().get(tables.getHref(), new TypeReference<Page<TableList>>() {});
    }
}
