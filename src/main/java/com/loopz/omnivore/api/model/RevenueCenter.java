package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.omnivore.api.model.embedded.RevenueCenterEmbedded;
import com.loopz.omnivore.api.model.link.RevenueCenterLinks;
import java.util.List;

@JsonDeserialize(builder = RevenueCenter.Builder.class)
public class RevenueCenter {

    private final String id;
    private final String name;
    private final String posId;
    private final boolean defaulted;
    private final RevenueCenterEmbedded embedded;
    private final RevenueCenterLinks links;

    private RevenueCenter(Builder builder) {
        id = builder.id;
        name = builder.name;
        posId = builder.posId;
        defaulted = builder.defaulted;
        embedded = builder.embedded;
        links = builder.links;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPosId() {
        return posId;
    }

    public boolean isDefaulted() {
        return defaulted;
    }

    public RevenueCenterLinks getLinks() {
        return links;
    }

    public List<Table> getTables() {
        return embedded.getTables();
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private String posId;
        private boolean defaulted;
        private RevenueCenterEmbedded embedded;
        private RevenueCenterLinks links;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        @JsonProperty("pos_id")
        public Builder posId(String posId) {
            this.posId = posId;
            return this;
        }

        @JsonProperty("default")
        public Builder defaulted(boolean defaulted) {
            this.defaulted = defaulted;
            return this;
        }

        @JsonProperty("_embedded")
        public Builder embedded(RevenueCenterEmbedded embedded) {
            this.embedded = embedded;
            return this;
        }

        @JsonProperty("_links")
        public Builder links(RevenueCenterLinks links) {
            this.links = links;
            return this;
        }

        public RevenueCenter build() {
            return new RevenueCenter(this);
        }
    }
}
