package com.loopz.omnivore.api.model.query;

public interface QueryFieldBuilder {

    QueryFieldBuilder eq(QueryField field, Object value);

    QueryFieldBuilder neq(QueryField field, Object value);

    QueryFieldBuilder gt(QueryField field, Object value);

    QueryFieldBuilder gte(QueryField field, Object value);

    QueryFieldBuilder lt(QueryField field, Object value);

    QueryFieldBuilder lte(QueryField field, Object value);

    QueryFieldBuilder in(QueryField field, Object... value);

    Query build();

}
