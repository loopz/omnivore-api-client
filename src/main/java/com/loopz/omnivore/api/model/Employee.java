package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.omnivore.api.model.link.EmployeeLinks;

@JsonDeserialize(builder = Employee.Builder.class)
public class Employee {

    private final String id;
    private final String posId;
    private final String login;
    private final String firstName;
    private final String lastName;
    private final EmployeeLinks links;

    private Employee(Builder builder) {
        id = builder.id;
        posId = builder.posId;
        login = builder.login;
        firstName = builder.firstName;
        lastName = builder.lastName;
        links = builder.links;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getPosId() {
        return posId;
    }

    public String getLogin() {
        return login;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public EmployeeLinks getLinks() {
        return links;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String posId;
        private String login;
        private String firstName;
        private String lastName;
        private EmployeeLinks links;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        @JsonProperty("pos_id")
        public Builder posId(String posId) {
            this.posId = posId;
            return this;
        }

        public Builder login(String login) {
            this.login = login;
            return this;
        }

        @JsonProperty("first_name")
        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        @JsonProperty("last_name")
        public Builder lastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        @JsonProperty("_links")
        public Builder links(EmployeeLinks links) {
            this.links = links;
            return this;
        }

        public Employee build() {
            return new Employee(this);
        }
    }
}
