package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Job;
import com.loopz.omnivore.api.model.Page;
import java.util.List;

public class JobList {

    private final List<Job> jobs;

    public JobList(@JsonProperty("jobs") List<Job> jobs) {
        this.jobs = jobs;
    }

    public List<Job> getJobs() {
        return jobs;
    }

    public static Page<JobList> list(String locationId) throws OmnivoreException {
        return OmnivoreApi.client().listJobs(locationId);
    }

}
