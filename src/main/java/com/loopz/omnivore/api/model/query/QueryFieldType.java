package com.loopz.omnivore.api.model.query;

public enum QueryFieldType {

    STRING,
    NUMBER,
    BOOLEAN,
    TIMESTAMP;


}
