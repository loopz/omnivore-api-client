package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = OptionSet.Builder.class)
public class OptionSet {

    private final String id;
    private final int minimum;
    private final int maximum;
    private final boolean required;

    private OptionSet(Builder builder) {
        id = builder.id;
        minimum = builder.minimum;
        maximum = builder.maximum;
        required = builder.required;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public int getMinimum() {
        return minimum;
    }

    public int getMaximum() {
        return maximum;
    }

    public boolean isRequired() {
        return required;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private int minimum;
        private int maximum;
        private boolean required;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder minimum(int minimum) {
            this.minimum = minimum;
            return this;
        }

        public Builder maximum(int maximum) {
            this.maximum = maximum;
            return this;
        }

        public Builder required(boolean required) {
            this.required = required;
            return this;
        }

        public OptionSet build() {
            return new OptionSet(this);
        }
    }
}
