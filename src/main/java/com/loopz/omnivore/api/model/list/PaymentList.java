package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Page;
import com.loopz.omnivore.api.model.Payment;
import java.util.List;

public class PaymentList {

    private final List<Payment> payments;

    public PaymentList(@JsonProperty("payments") List<Payment> payments) {
        this.payments = payments;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public static Page<PaymentList> list(String locationId, String ticketId) throws OmnivoreException {
        return OmnivoreApi.client().listPayments(locationId, ticketId);
    }

}
