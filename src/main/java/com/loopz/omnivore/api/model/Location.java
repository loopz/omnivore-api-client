package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.link.LocationLinks;
import java.time.Instant;

@JsonDeserialize(builder = Location.Builder.class)
public class Location {

    private final String id;
    private final String name;
    private final String phone;
    private final String website;
    private final String posType;
    private final String status;
    private final String owner;
    private final Instant created;
    private final Instant modified;
    private final String displayName;
    private final String conceptName;
    private final boolean development;
    private final Address address;
    private final LocationLinks links;

    private Location(Builder builder) {
        id = builder.id;
        name = builder.name;
        phone = builder.phone;
        website = builder.website;
        posType = builder.posType;
        status = builder.status;
        owner = builder.owner;
        created = builder.created;
        modified = builder.modified;
        displayName = builder.displayName;
        conceptName = builder.conceptName;
        development = builder.development;
        address = builder.address;
        links = builder.links;
    }

    public static Location get(String locationId) throws OmnivoreException {
        return OmnivoreApi.client().getLocation(locationId);
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getWebsite() {
        return website;
    }

    public String getPosType() {
        return posType;
    }

    public String getStatus() {
        return status;
    }

    public String getOwner() {
        return owner;
    }

    public Instant getCreated() {
        return created;
    }

    public Instant getModified() {
        return modified;
    }

    public String getDisplayName() {
        return displayName;
    }

    public String getConceptName() {
        return conceptName;
    }

    public boolean isDevelopment() {
        return development;
    }

    public Address getAddress() {
        return address;
    }

    public LocationLinks getLinks() {
        return links;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private String phone;
        private String website;
        private String posType;
        private String status;
        private String owner;
        private Instant created;
        private Instant modified;
        private String displayName;
        private String conceptName;
        private boolean development;
        private Address address;
        private LocationLinks links;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder phone(String phone) {
            this.phone = phone;
            return this;
        }

        public Builder website(String website) {
            this.website = website;
            return this;
        }

        @JsonProperty("pos_type")
        public Builder posType(String posType) {
            this.posType = posType;
            return this;
        }

        public Builder status(String status) {
            this.status = status;
            return this;
        }

        public Builder owner(String owner) {
            this.owner = owner;
            return this;
        }

        public Builder created(Instant created) {
            this.created = created;
            return this;
        }

        public Builder modified(Instant modified) {
            this.modified = modified;
            return this;
        }

        @JsonProperty("display_name")
        public Builder displayName(String displayName) {
            this.displayName = displayName;
            return this;
        }

        @JsonProperty("concept_name")
        public Builder conceptName(String conceptName) {
            this.conceptName = conceptName;
            return this;
        }

        public Builder development(boolean development) {
            this.development = development;
            return this;
        }

        public Builder address(Address address) {
            this.address = address;
            return this;
        }

        @JsonProperty("_links")
        public Builder links(LocationLinks links) {
            this.links = links;
            return this;
        }

        public Location build() {
            return new Location(this);
        }
    }
}
