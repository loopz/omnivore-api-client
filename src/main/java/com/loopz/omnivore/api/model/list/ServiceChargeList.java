package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Page;
import com.loopz.omnivore.api.model.ServiceCharge;
import java.util.List;

public class ServiceChargeList {

    private final List<ServiceCharge> serviceCharges;

    public ServiceChargeList(@JsonProperty("service_charges") List<ServiceCharge> serviceCharges) {
        this.serviceCharges = serviceCharges;
    }

    public List<ServiceCharge> getServiceCharges() {
        return serviceCharges;
    }

    public static Page<ServiceChargeList> list(String locationId, String ticketId) throws OmnivoreException {
        return OmnivoreApi.client().listServiceCharges(locationId, ticketId);
    }

}
