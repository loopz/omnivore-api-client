package com.loopz.omnivore.api.model.query;

import java.util.ArrayList;
import java.util.List;

public class Query {

    private BooleanOperator booleanOperator;
    private List<QueryBuilder.QueryFieldClause> queryFieldClauses = new ArrayList<>();

    public Query(BooleanOperator booleanOperator, List<QueryBuilder.QueryFieldClause> queryFieldClauses) {
        this.booleanOperator = booleanOperator;
        this.queryFieldClauses = queryFieldClauses;
    }

    @Override
    public String toString() {
        final StringBuilder queryBuilder = new StringBuilder("where=");

        if(queryFieldClauses.size() > 1) {
            queryBuilder.append(booleanOperator.getOperator()).append("(");
        }


        for (int i = 0; i < queryFieldClauses.size(); i++) {
            QueryBuilder.QueryFieldClause queryFieldClause = queryFieldClauses.get(i);
            queryBuilder.append(queryFieldClause.getOperator().getOperator()).append("(");
            queryBuilder.append(queryFieldClause.getField().getName()).append(",");
            queryBuilder.append(queryFieldClause.getValueAsString());
            queryBuilder.append(")");
            if (i < queryFieldClauses.size() - 1) {
                queryBuilder.append(",");
            }
        }

        if(queryFieldClauses.size() > 1) {
            queryBuilder.append(")");
        }

        return queryBuilder.toString();
    }
}
