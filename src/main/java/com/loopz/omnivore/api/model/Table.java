package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.omnivore.api.model.embedded.TableEmbedded;
import com.loopz.omnivore.api.model.link.TableLinks;

@JsonDeserialize(builder = Table.Builder.class)
public class Table {

    private final String id;
    private final String name;
    private final String posId;
    private final int number;
    private final int seats;
    private final boolean available;
    private final TableEmbedded embedded;
    private final TableLinks links;

    private Table(Builder builder) {
        id = builder.id;
        name = builder.name;
        posId = builder.posId;
        number = builder.number;
        seats = builder.seats;
        available = builder.available;
        embedded = builder.embedded;
        links = builder.links;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPosId() {
        return posId;
    }

    public int getNumber() {
        return number;
    }

    public int getSeats() {
        return seats;
    }

    public boolean isAvailable() {
        return available;
    }

    public TableLinks getLinks() {
        return links;
    }

    public RevenueCenter getRevenueCenter() {
        return embedded.getRevenueCenter();
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private String posId;
        private int number;
        private int seats;
        private boolean available;
        private TableEmbedded embedded;
        private TableLinks links;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        @JsonProperty("pos_id")
        public Builder posId(String posId) {
            this.posId = posId;
            return this;
        }

        public Builder number(int number) {
            this.number = number;
            return this;
        }

        public Builder seats(int seats) {
            this.seats = seats;
            return this;
        }

        public Builder available(boolean available) {
            this.available = available;
            return this;
        }

        @JsonProperty("_embedded")
        public Builder embedded(TableEmbedded embedded) {
            this.embedded = embedded;
            return this;
        }

        @JsonProperty("_links")
        public Builder links(TableLinks links) {
            this.links = links;
            return this;
        }

        public Table build() {
            return new Table(this);
        }
    }
}
