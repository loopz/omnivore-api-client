package com.loopz.omnivore.api.model.query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class QueryBuilder implements QueryFieldBuilder {

    private BooleanOperator booleanOperator;
    private List<QueryFieldClause> queryFieldClauses = new ArrayList<>();

    public QueryBuilder and() {
        booleanOperator = BooleanOperator.AND;
        return this;
    }

    public QueryBuilder or() {
        booleanOperator = BooleanOperator.OR;
        return this;
    }

    public QueryBuilder not() {
        booleanOperator = BooleanOperator.NOT;
        return this;
    }

    @Override
    public QueryFieldBuilder eq(QueryField field, Object value) {
        return addQueryFieldClause(ComparisonOperator.EQUAL, field, value);
    }

    @Override
    public QueryFieldBuilder neq(QueryField field, Object value) {
        return addQueryFieldClause(ComparisonOperator.NOT_EQUAL, field, value);
    }

    @Override
    public QueryFieldBuilder gt(QueryField field, Object value) {
        return addQueryFieldClause(ComparisonOperator.GREATER_THAN, field, value);
    }

    @Override
    public QueryFieldBuilder gte(QueryField field, Object value) {
        return addQueryFieldClause(ComparisonOperator.GREATER_THAN_OR_EQUAL, field, value);
    }

    @Override
    public QueryFieldBuilder lt(QueryField field, Object value) {
        return addQueryFieldClause(ComparisonOperator.LESS_THAN, field, value);
    }

    @Override
    public QueryFieldBuilder lte(QueryField field, Object value) {
        return addQueryFieldClause(ComparisonOperator.LESS_THAN_OR_EQUAL, field, value);
    }

    @Override
    public QueryFieldBuilder in(QueryField field, Object... value) {
        return addQueryFieldClause(ComparisonOperator.IN, field, value);
    }

    @Override
    public Query build() {
        return new Query(booleanOperator, queryFieldClauses);
    }

    private QueryFieldBuilder addQueryFieldClause(ComparisonOperator operator, QueryField field, Object... values) {
        queryFieldClauses.add(new QueryFieldClause(operator, field, values));
        return this;
    }

    public class QueryFieldClause {

        private final ComparisonOperator operator;

        private final QueryField field;

        private List<Object> values = new ArrayList<>();

        public QueryFieldClause(ComparisonOperator operator, QueryField field, Object... values) {
            this.operator = operator;
            this.field = field;
            this.values = Arrays.asList(values);
        }

        public ComparisonOperator getOperator() {
            return operator;
        }

        public QueryField getField() {
            return field;
        }

        public List<Object> getValues() {
            return values;
        }

        public String getValueAsString() {
            StringBuilder valueBuilder = new StringBuilder();
            Iterator<Object> iterator = values.iterator();
            while (iterator.hasNext()) {
                Object value = iterator.next();
                if(value instanceof String) {
                    valueBuilder.append("'").append(value).append("'");
                } else {
                    valueBuilder.append(value);
                }
                if (iterator.hasNext()) {
                    valueBuilder.append(",");
                }
            }
            return valueBuilder.toString();
        }
    }

}
