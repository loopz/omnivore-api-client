package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Page;
import com.loopz.omnivore.api.model.PriceLevel;
import java.util.List;

public class PriceLevelList {

    private final List<PriceLevel> priceLevels;

    public PriceLevelList(@JsonProperty("price_levels") List<PriceLevel> priceLevels) {
        this.priceLevels = priceLevels;
    }

    public List<PriceLevel> getOrderTypes() {
        return priceLevels;
    }

    public static Page<PriceLevelList> list(String locationId, String menuItemId) throws OmnivoreException {
        return OmnivoreApi.client().listPriceLevels(locationId, menuItemId);
    }

}
