package com.loopz.omnivore.api.model.request;

/**
 * Base class for actual payment requests
 */
public abstract class MakePaymentRequest {

    private final String type;
    private final int tip;
    private final String comment;
    private final int amount;
    private final boolean autoClose;

    protected MakePaymentRequest(AbstractBuilder builder) {
        type = getType();
        tip = builder.tip;
        comment = builder.comment;
        amount = builder.amount;
        autoClose = builder.autoClose;
    }

    public int getTip() {
        return tip;
    }

//    public String getComment() {
//        return comment;
//    }

    public int getAmount() {
        return amount;
    }

//    @JsonProperty("auto_close")
//    public boolean isAutoClose() {
//        return autoClose;
//    }

    public abstract String getType();

    protected static abstract class AbstractBuilder<T extends MakePaymentRequest, B extends AbstractBuilder> {
        private int tip;
        private String comment;
        private int amount;
        private boolean autoClose;

        protected AbstractBuilder() {
        }

        public B tip(int tip) {
            this.tip = tip;
            return (B) this;
        }

        public B comment(String comment) {
            this.comment = comment;
            return (B) this;
        }

        public B amount(int amount) {
            this.amount = amount;
            return (B) this;
        }

        public B autoClose(boolean autoClose) {
            this.autoClose = autoClose;
            return (B) this;
        }

        public abstract T build();
    }
}
