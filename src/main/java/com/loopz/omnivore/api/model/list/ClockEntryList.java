package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.model.ClockEntry;
import java.util.List;

public class ClockEntryList {

    private final List<ClockEntry> clockEntries;

    public ClockEntryList(@JsonProperty("clock_entries") List<ClockEntry> clockEntries) {
        this.clockEntries = clockEntries;
    }

    public List<ClockEntry> getClockEntries() {
        return clockEntries;
    }

}
