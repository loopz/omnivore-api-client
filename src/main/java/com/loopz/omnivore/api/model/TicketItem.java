package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = TicketItem.Builder.class)
public class TicketItem {

    private final String id;
    private final String name;
    private final String comment;
    private final int pricePerUnit;
    private final int quantity;
    private final boolean sent;
    private final MenuItem menuItem;

    private TicketItem(Builder builder) {
        id = builder.id;
        name = builder.name;
        comment = builder.comment;
        pricePerUnit = builder.pricePerUnit;
        quantity = builder.quantity;
        sent = builder.sent;
        menuItem = builder.menuItem;
    }

    public static Builder builder() {
        return new Builder();
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private String comment;
        private int pricePerUnit;
        private int quantity;
        private boolean sent;
        private MenuItem menuItem;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder comment(String comment) {
            this.comment = comment;
            return this;
        }

        @JsonProperty("price_per_unit")
        public Builder pricePerUnit(int pricePerUnit) {
            this.pricePerUnit = pricePerUnit;
            return this;
        }

        public Builder quantity(int quantity) {
            this.quantity = quantity;
            return this;
        }

        public Builder sent(boolean sent) {
            this.sent = sent;
            return this;
        }

        @JsonProperty("_embedded")
        public Builder menuItem(MenuItem menuItem){
            this.menuItem = menuItem;
            return this;
        }

        public TicketItem build() {
            return new TicketItem(this);
        }
    }
}
