package com.loopz.omnivore.api.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MakeGiftCardPaymentRequest extends MakePaymentRequest {

    private final CardInfo cardInfo;

    protected MakeGiftCardPaymentRequest(Builder builder) {
        super(builder);
        cardInfo = builder.cardInfo;
    }

    @Override
    public String getType() {
        return "gift_card";
    }

    @JsonProperty("card_info")
    public CardInfo getCardInfo() {
        return cardInfo;
    }

    public static final class Builder extends AbstractBuilder<MakeGiftCardPaymentRequest, Builder> {

        private CardInfo cardInfo;

        public Builder cardInfo(CardInfo cardInfo) {
            this.cardInfo = cardInfo;
            return this;
        }

        @Override
        public MakeGiftCardPaymentRequest build() {
            return new MakeGiftCardPaymentRequest(this);
        }
    }

    public static final class CardInfo {

        private final String number;

        public CardInfo(String number) {
            this.number = number;
        }

        public String getNumber() {
            return number;
        }
    }
}
