package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.OrderType;
import com.loopz.omnivore.api.model.Page;
import java.util.List;

public class OrderTypeList {

    private final List<OrderType> orderTypes;

    public OrderTypeList(@JsonProperty("order_types") List<OrderType> orderTypes) {
        this.orderTypes = orderTypes;
    }

    public List<OrderType> getOrderTypes() {
        return orderTypes;
    }

    public static Page<OrderTypeList> list(String locationId) throws OmnivoreException {
        return OmnivoreApi.client().listOrderTypes(locationId);
    }

}
