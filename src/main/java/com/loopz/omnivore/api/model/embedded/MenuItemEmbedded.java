package com.loopz.omnivore.api.model.embedded;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.omnivore.api.model.Category;
import com.loopz.omnivore.api.model.OptionSet;
import com.loopz.omnivore.api.model.PriceLevel;
import java.util.List;

@JsonDeserialize(builder = MenuItemEmbedded.Builder.class)
public class MenuItemEmbedded {

    private final List<Category> categories;
    private final List<OptionSet> optionSets;
    private final List<PriceLevel> priceLevels;

    private MenuItemEmbedded(Builder builder) {
        categories = builder.categories;
        optionSets = builder.optionSets;
        priceLevels = builder.priceLevels;
    }

    public static Builder builder() {
        return new Builder();
    }

    public List<Category> getCategories() {
        return categories;
    }

    public List<OptionSet> getOptionSets() {
        return optionSets;
    }

    public List<PriceLevel> getPriceLevels() {
        return priceLevels;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private List<Category> categories;
        private List<OptionSet> optionSets;
        private List<PriceLevel> priceLevels;

        private Builder() {
        }

        public Builder categories(List<Category> categories) {
            this.categories = categories;
            return this;
        }

        @JsonProperty("option_sets")
        public Builder optionSets(List<OptionSet> optionSets) {
            this.optionSets = optionSets;
            return this;
        }

        @JsonProperty("price_levels")
        public Builder priceLevels(List<PriceLevel> priceLevels) {
            this.priceLevels = priceLevels;
            return this;
        }

        public MenuItemEmbedded build() {
            return new MenuItemEmbedded(this);
        }
    }
}
