package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Page;
import com.loopz.omnivore.api.model.TicketDiscount;
import java.util.List;

public class TicketDiscountList {

    private final List<TicketDiscount> discounts;

    public TicketDiscountList(@JsonProperty("discounts") List<TicketDiscount> discounts) {
        this.discounts = discounts;
    }

    public List<TicketDiscount> getDiscounts() {
        return discounts;
    }

    public static Page<TicketDiscountList> list(String locationId, String ticketId) throws OmnivoreException {
        return OmnivoreApi.client().listTicketDiscounts(locationId, ticketId);
    }
}
