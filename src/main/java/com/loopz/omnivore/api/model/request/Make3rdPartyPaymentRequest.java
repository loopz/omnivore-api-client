package com.loopz.omnivore.api.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Make3rdPartyPaymentRequest extends MakePaymentRequest{

    private final String tenderType;

    private Make3rdPartyPaymentRequest(Builder builder) {
        super(builder);
        tenderType = builder.tenderType;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String getType() {
        return "3rd_party";
    }

    @JsonProperty("tender_type")
    public String getTenderType() {
        return tenderType;
    }

    public static final class Builder extends AbstractBuilder<Make3rdPartyPaymentRequest, Builder> {
        private String tenderType;

        private Builder() {
        }

        public Builder tenderType(String tenderType) {
            this.tenderType = tenderType;
            return this;
        }

        @Override
        public Make3rdPartyPaymentRequest build() {
            return new Make3rdPartyPaymentRequest(this);
        }
    }
}
