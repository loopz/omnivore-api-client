package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = PriceLevel.Builder.class)
public class PriceLevel {

    private String id;
    private String name;
    private int pricePerUnit;

    private PriceLevel(Builder builder) {
        id = builder.id;
        name = builder.name;
        pricePerUnit = builder.pricePerUnit;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPricePerUnit() {
        return pricePerUnit;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private int pricePerUnit;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        @JsonProperty("price_per_unit")
        public Builder pricePerUnit(int pricePerUnit) {
            this.pricePerUnit = pricePerUnit;
            return this;
        }

        public PriceLevel build() {
            return new PriceLevel(this);
        }
    }
}
