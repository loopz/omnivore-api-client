package com.loopz.omnivore.api.model.link;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Represents a link to a model that can be fetched from the API using the {@link #get()} method.
 */
public class Link<T> {

    private final String href;
    private final String type;

    public Link(@JsonProperty("href") String href, @JsonProperty("type") String type) {
        this.href = href;
        this.type = type;
    }

    public String getHref() {
        return href;
    }

    public String getType() {
        return type;
    }

}
