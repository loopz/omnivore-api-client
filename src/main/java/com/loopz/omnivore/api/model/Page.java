package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Page<T> {

    private final T list;
    private final int start;
    private final int limit;
    private final int count;

    public Page(
            @JsonProperty("_embedded") T list,
            @JsonProperty("start") int start,
            @JsonProperty("limit") int limit,
            @JsonProperty("count") int count) {
        this.list = list;
        this.start = start;
        this.limit = limit;
        this.count = count;
    }

    public T getList() {
        return list;
    }

    public int getStart() {
        return start;
    }

    public int getLimit() {
        return limit;
    }

    public int getCount() {
        return count;
    }
}
