package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Page;
import com.loopz.omnivore.api.model.RevenueCenter;
import java.util.List;

public class RevenueCenterList {

    private final List<RevenueCenter> revenueCenters;

    public RevenueCenterList(@JsonProperty("revenue_centers") List<RevenueCenter> revenueCenters) {
        this.revenueCenters = revenueCenters;
    }

    public List<RevenueCenter> getRevenueCenters() {
        return revenueCenters;
    }

    public static Page<RevenueCenterList> list(String locationId) throws OmnivoreException {
        return OmnivoreApi.client().listRevenueCenters(locationId);
    }

}
