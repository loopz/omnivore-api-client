package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.request.Make3rdPartyPaymentRequest;
import com.loopz.omnivore.api.model.request.MakePaymentRequest;

@JsonDeserialize(builder = Payment.Builder.class)
public class Payment {

    private final String id;
    private final String type;
    private final String comment;
    private final String last4;
    private final int tip;
    private final int change;
    private final int amount;

    private Payment(Builder builder) {
        id = builder.id;
        type = builder.type;
        comment = builder.comment;
        last4 = builder.last4;
        tip = builder.tip;
        change = builder.change;
        amount = builder.amount;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Payment get(String locationId, String ticketId, String paymentId) throws OmnivoreException {
        return OmnivoreApi.client().getPayment(locationId, ticketId, paymentId);
    }

    public static Payment make(String locationId, String ticketId, MakePaymentRequest paymentRequest) throws OmnivoreException {
        return OmnivoreApi.client().makePayment(locationId, ticketId, paymentRequest);
    }

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getComment() {
        return comment;
    }

    public String getLast4() {
        return last4;
    }

    public int getTip() {
        return tip;
    }

    public int getChange() {
        return change;
    }

    public int getAmount() {
        return amount;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String type;
        private String comment;
        private String last4;
        private int tip;
        private int change;
        private int amount;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        public Builder comment(String comment) {
            this.comment = comment;
            return this;
        }

        public Builder last4(String last4) {
            this.last4 = last4;
            return this;
        }

        public Builder tip(int tip) {
            this.tip = tip;
            return this;
        }

        public Builder change(int change) {
            this.change = change;
            return this;
        }

        public Builder amount(int amount) {
            this.amount = amount;
            return this;
        }

        public Payment build() {
            return new Payment(this);
        }
    }
}
