package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Location;
import com.loopz.omnivore.api.model.Page;
import java.util.List;

public class LocationList {

    private final List<Location> locations;

    public LocationList(@JsonProperty("locations") List<Location> locations) {
        this.locations = locations;
    }

    public List<Location> getLocations() {
        return locations;
    }

    public static Page<LocationList> list() throws OmnivoreException {
        return OmnivoreApi.client().listLocations();
    }
}
