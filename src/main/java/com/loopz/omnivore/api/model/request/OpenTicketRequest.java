package com.loopz.omnivore.api.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class OpenTicketRequest {

    private final String tableId;
    private final String revenueCenterId;
    private final String orderTypeId;
    private final String name;
    private final int guestCount;
    private final String employeeId;
    private final boolean autoSend;

    private OpenTicketRequest(Builder builder) {
        tableId = builder.tableId;
        revenueCenterId = builder.revenueCenterId;
        orderTypeId = builder.orderTypeId;
        name = builder.name;
        guestCount = builder.guestCount;
        employeeId = builder.employeeId;
        autoSend = builder.autoSend;
    }

    @JsonProperty("table")
    public String getTableId() {
        return tableId;
    }

    @JsonProperty("revenue_center")
    public String getRevenueCenterId() {
        return revenueCenterId;
    }

    @JsonProperty("order_type")
    public String getOrderTypeId() {
        return orderTypeId;
    }

    public String getName() {
        return name;
    }

    @JsonProperty("guest_count")
    public int getGuestCount() {
        return guestCount;
    }

    @JsonProperty("employee")
    public String getEmployeeId() {
        return employeeId;
    }

    @JsonProperty("auto_send")
    public boolean isAutoSend() {
        return autoSend;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static final class Builder {
        private String tableId;
        private String revenueCenterId;
        private String orderTypeId;
        private String name;
        private int guestCount;
        private String employeeId;
        private boolean autoSend;

        private Builder() {
        }

        public Builder tableId(String tableId) {
            this.tableId = tableId;
            return this;
        }

        public Builder revenueCenterId(String revenueCenterId) {
            this.revenueCenterId = revenueCenterId;
            return this;
        }

        public Builder orderTypeId(String orderTypeId) {
            this.orderTypeId = orderTypeId;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder guestCount(int guestCount) {
            this.guestCount = guestCount;
            return this;
        }

        public Builder employeeId(String employeeId) {
            this.employeeId = employeeId;
            return this;
        }

        public Builder autoSend(boolean autoSend) {
            this.autoSend = autoSend;
            return this;
        }

        public OpenTicketRequest build() {
            return new OpenTicketRequest(this);
        }
    }
}
