package com.loopz.omnivore.api.model.link;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Page;
import com.loopz.omnivore.api.model.RevenueCenter;
import com.loopz.omnivore.api.model.TicketList;

public class TableLinks {

    private final Link<Page<TicketList>> openTickets;
    private final Link<RevenueCenter> revenueCenter;

    public TableLinks(
            @JsonProperty("open_tickets") Link<Page<TicketList>> openTickets,
            @JsonProperty("revenue_center") Link<RevenueCenter> revenueCenter) {
        this.openTickets = openTickets;
        this.revenueCenter = revenueCenter;
    }

    public Page<TicketList> getOpenTickets() throws OmnivoreException {
        return OmnivoreApi.client().get(openTickets.getHref(), new TypeReference<Page<TicketList>>() {});
    }

    public RevenueCenter getRevenueCenter() throws OmnivoreException {
        if(revenueCenter == null) {
            return null;
        }
        return OmnivoreApi.client().get(revenueCenter.getHref(), new TypeReference<RevenueCenter>() {});
    }
}
