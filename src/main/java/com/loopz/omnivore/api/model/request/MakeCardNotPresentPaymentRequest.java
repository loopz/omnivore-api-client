package com.loopz.omnivore.api.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MakeCardNotPresentPaymentRequest extends MakePaymentRequest {

    private final CardInfo cardInfo;

    public MakeCardNotPresentPaymentRequest(Builder builder) {
        super(builder);
        cardInfo = builder.cardInfo;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String getType() {
        return "card_not_present";
    }

    @JsonProperty("card_info")
    public CardInfo getCardInfo() {
        return cardInfo;
    }

    public static final class Builder extends AbstractBuilder<MakeCardNotPresentPaymentRequest, Builder> {

        private CardInfo cardInfo;

        private Builder() {}

        public CardInfo.Builder cardInfo() {
            return new CardInfo.Builder(this);
        }

        @Override
        public MakeCardNotPresentPaymentRequest build() {
            return new MakeCardNotPresentPaymentRequest(this);
        }
    }

    public static final class CardInfo {

        private final String number;
        private final int expYear;
        private final int expMonth;
        private final String cvc2;

        private CardInfo(CardInfo.Builder builder) {
            number = builder.number;
            expYear = builder.expYear;
            expMonth = builder.expMonth;
            cvc2 = builder.cvc2;
        }

        public String getNumber() {
            return number;
        }

        @JsonProperty("exp_year")
        public int getExpYear() {
            return expYear;
        }

        @JsonProperty("exp_month")
        public int getExpMonth() {
            return expMonth;
        }

        public String getCvc2() {
            return cvc2;
        }

        public static final class Builder {

            private final MakeCardNotPresentPaymentRequest.Builder builder;
            private String number;
            private int expYear;
            private int expMonth;
            private String cvc2;

            public Builder(MakeCardNotPresentPaymentRequest.Builder builder) {
                this.builder = builder;
            }

            public Builder number(String number) {
                this.number = number;
                return this;
            }

            public Builder expYear(int expYear) {
                this.expYear = expYear;
                return this;
            }

            public Builder expMonth(int expMonth) {
                this.expMonth = expMonth;
                return this;
            }

            public Builder cvc2(String cvc2) {
                this.cvc2 = cvc2;
                return this;
            }

            public MakeCardNotPresentPaymentRequest.Builder done() {
                builder.cardInfo = new CardInfo(this);
                return builder;
            }
        }
    }
}
