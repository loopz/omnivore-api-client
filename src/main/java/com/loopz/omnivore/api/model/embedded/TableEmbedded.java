package com.loopz.omnivore.api.model.embedded;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.model.RevenueCenter;

public class TableEmbedded {

    private final RevenueCenter revenueCenter;

    public TableEmbedded(@JsonProperty("revenue_center") RevenueCenter revenueCenter) {
        this.revenueCenter = revenueCenter;
    }

    public RevenueCenter getRevenueCenter() {
        return revenueCenter;
    }
}
