package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = TicketDiscount.Builder.class)
public class TicketDiscount {

    private final String id;
    private final String name;
    private final String comment;
    private final int value;

    private TicketDiscount(Builder builder) {
        id = builder.id;
        name = builder.name;
        comment = builder.comment;
        value = builder.value;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getComment() {
        return comment;
    }

    public int getValue() {
        return value;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private String comment;
        private int value;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder comment(String comment) {
            this.comment = comment;
            return this;
        }

        public Builder value(int value) {
            this.value = value;
            return this;
        }

        public TicketDiscount build() {
            return new TicketDiscount(this);
        }
    }
}
