package com.loopz.omnivore.api.model.link;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Category;

public class CategoryLinks {

    private final Link<Category> parentCategory;

    public CategoryLinks(
            @JsonProperty("category") Link<Category> parentCategory) {
        this.parentCategory = parentCategory;
    }

    public Category getParentCategory() throws OmnivoreException {
        return OmnivoreApi.client().get(parentCategory.getHref(), new TypeReference<Category>() {});
    }
}
