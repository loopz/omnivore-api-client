package com.loopz.omnivore.api.model.embedded;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.omnivore.api.model.*;
import java.util.List;

@JsonDeserialize(builder = TicketEmbedded.Builder.class)
public class TicketEmbedded {

    private final List<TicketDiscount> discounts;
    private final List<Payment> payments;
    private final Employee employee;
    private final List<TicketItem> items;
    private final List<TicketItem> voidedItems;
    private final OrderType orderType;
    private final RevenueCenter revenueCenter;
    private final Table table;
    private final List<ServiceCharge> serviceCharges;

    private TicketEmbedded(Builder builder) {
        discounts = builder.discounts;
        payments = builder.payments;
        employee = builder.employee;
        items = builder.items;
        voidedItems = builder.voidedItems;
        orderType = builder.orderType;
        revenueCenter = builder.revenueCenter;
        table = builder.table;
        serviceCharges = builder.serviceCharges;
    }

    public static Builder builder() {
        return new Builder();
    }

    public List<TicketDiscount> getDiscounts() {
        return discounts;
    }

    public List<Payment> getPayments() {
        return payments;
    }

    public Employee getEmployee() {
        return employee;
    }

    public List<TicketItem> getItems() {
        return items;
    }

    public List<TicketItem> getVoidedItems() {
        return voidedItems;
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public RevenueCenter getRevenueCenter() {
        return revenueCenter;
    }

    public Table getTable() {
        return table;
    }

    public List<ServiceCharge> getServiceCharges() {
        return serviceCharges;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private List<TicketDiscount> discounts;
        private List<Payment> payments;
        private Employee employee;
        private List<TicketItem> items;
        private List<TicketItem> voidedItems;
        private OrderType orderType;
        private RevenueCenter revenueCenter;
        private Table table;
        private List<ServiceCharge> serviceCharges;

        private Builder() {
        }

        public Builder discounts(List<TicketDiscount> discounts) {
            this.discounts = discounts;
            return this;
        }

        public Builder payments(List<Payment> payments) {
            this.payments = payments;
            return this;
        }

        public Builder employee(Employee employee) {
            this.employee = employee;
            return this;
        }

        public Builder items(List<TicketItem> items) {
            this.items = items;
            return this;
        }

        @JsonProperty("voided_items")
        public Builder voidedItems(List<TicketItem> voidedItems) {
            this.voidedItems = voidedItems;
            return this;
        }

        @JsonProperty("order_type")
        public Builder orderType(OrderType orderType) {
            this.orderType = orderType;
            return this;
        }

        @JsonProperty("revenue_center")
        public Builder revenueCenter(RevenueCenter revenueCenter) {
            this.revenueCenter = revenueCenter;
            return this;
        }

        public Builder table(Table table) {
            this.table = table;
            return this;
        }

        @JsonProperty("service_charges")
        public Builder serviceCharges(List<ServiceCharge> serviceCharges) {
            this.serviceCharges = serviceCharges;
            return this;
        }

        public TicketEmbedded build() {
            return new TicketEmbedded(this);
        }
    }
}
