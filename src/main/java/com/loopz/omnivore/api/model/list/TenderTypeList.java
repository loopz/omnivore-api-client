package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Page;
import com.loopz.omnivore.api.model.TenderType;
import java.util.List;

public class TenderTypeList {

    private final List<TenderType> tenderTypes;

    public TenderTypeList(@JsonProperty("tender_types") List<TenderType> tenderTypes) {
        this.tenderTypes = tenderTypes;
    }

    public List<TenderType> getTenderTypes() {
        return tenderTypes;
    }

    public static Page<TenderTypeList> list(String locationId) throws OmnivoreException {
        return OmnivoreApi.client().listTenderTypes(locationId);
    }

}
