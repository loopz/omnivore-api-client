package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = Address.Builder.class)
public class Address {

    private final String street1;
    private final String street2;
    private final String city;
    private final String state;
    private final String zip;

    private Address(Builder builder) {
        street1 = builder.street1;
        street2 = builder.street2;
        city = builder.city;
        state = builder.state;
        zip = builder.zip;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getStreet1() {
        return street1;
    }

    public String getStreet2() {
        return street2;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getZip() {
        return zip;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String street1;
        private String street2;
        private String city;
        private String state;
        private String zip;

        private Builder() {
        }

        public Builder street1(String street1) {
            this.street1 = street1;
            return this;
        }

        public Builder street2(String street2) {
            this.street2 = street2;
            return this;
        }

        public Builder city(String city) {
            this.city = city;
            return this;
        }

        public Builder state(String state) {
            this.state = state;
            return this;
        }

        public Builder zip(String zip) {
            this.zip = zip;
            return this;
        }

        public Address build() {
            return new Address(this);
        }
    }
}
