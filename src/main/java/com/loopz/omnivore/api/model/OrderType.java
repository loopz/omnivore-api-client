package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = OrderType.Builder.class)
public class OrderType {

    private final String id;
    private final String name;
    private final String posId;
    private final boolean available;

    private OrderType(Builder builder) {
        id = builder.id;
        name = builder.name;
        posId = builder.posId;
        available = builder.available;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPosId() {
        return posId;
    }

    public boolean isAvailable() {
        return available;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private String posId;
        private boolean available;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        @JsonProperty("pos_id")
        public Builder posId(String posId) {
            this.posId = posId;
            return this;
        }

        public Builder available(boolean available) {
            this.available = available;
            return this;
        }

        public OrderType build() {
            return new OrderType(this);
        }
    }
}
