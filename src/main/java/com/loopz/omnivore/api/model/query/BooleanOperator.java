package com.loopz.omnivore.api.model.query;

public enum BooleanOperator {

    AND("and"),
    OR("or"),
    NOT("not");

    private final String operator;

    BooleanOperator(String operator) {
        this.operator = operator;
    }

    public String getOperator() {
        return operator;
    }
}
