package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.model.Category;
import java.util.List;

public class CategoryList {

    private final List<Category> categories;

    public CategoryList(@JsonProperty("categories") List<Category> categories) {
        this.categories = categories;
    }

    public List<Category> getCategories() {
        return categories;
    }
}
