package com.loopz.omnivore.api.model.link;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Page;
import com.loopz.omnivore.api.model.TicketList;
import com.loopz.omnivore.api.model.list.*;

@JsonDeserialize(builder = LocationLinks.Builder.class)
public class LocationLinks {

    private final Link<Page<DiscountList>> discounts;
    private final Link<Page<TicketList>> tickets;
    private final Link<Page<ClockEntryList>> clockEntries;
    private final Link<Page<EmployeeList>> employees;
    private final Link<Page<JobList>> jobs;
    private final Link<Page<OrderTypeList>> orderTypes;
    private final Link<Page<RevenueCenterList>> revenueCenters;
    private final Link<Page<TableList>> tables;
    private final Link<Page<TenderTypeList>> tenderTypes;

    private LocationLinks(Builder builder) {
        discounts = builder.discounts;
        tickets = builder.tickets;
        clockEntries = builder.clockEntries;
        employees = builder.employees;
        jobs = builder.jobs;
        orderTypes = builder.orderTypes;
        revenueCenters = builder.revenueCenters;
        tables = builder.tables;
        tenderTypes = builder.tenderTypes;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Page<DiscountList> getDiscounts() throws OmnivoreException {
        return OmnivoreApi.client().get(discounts.getHref(), new TypeReference<Page<DiscountList>>() {});
    }

    public Page<TicketList> getTickets() throws OmnivoreException {
        return OmnivoreApi.client().get(tickets.getHref(), new TypeReference<Page<TicketList>>() {});
    }

    public Page<ClockEntryList> getClockEntries() throws OmnivoreException {
        return OmnivoreApi.client().get(clockEntries.getHref(), new TypeReference<Page<ClockEntryList>>() {});
    }

    public Page<EmployeeList> getEmployees() throws OmnivoreException {
        return OmnivoreApi.client().get(employees.getHref(), new TypeReference<Page<EmployeeList>>() {});
    }

    public Page<JobList> getJobs() throws OmnivoreException {
        return OmnivoreApi.client().get(jobs.getHref(), new TypeReference<Page<JobList>>() {});
    }

    public Page<OrderTypeList> getOrderTypes() throws OmnivoreException {
        return OmnivoreApi.client().get(orderTypes.getHref(), new TypeReference<Page<OrderTypeList>>() {});
    }

    public Page<RevenueCenterList> getRevenueCenters() throws OmnivoreException {
        return OmnivoreApi.client().get(revenueCenters.getHref(), new TypeReference<Page<RevenueCenterList>>() {});
    }

    public Page<TableList> getTables() throws OmnivoreException {
        return OmnivoreApi.client().get(tables.getHref(), new TypeReference<Page<TableList>>() {});
    }

    public Page<TenderTypeList> getTenderTypes() throws OmnivoreException {
        return OmnivoreApi.client().get(tenderTypes.getHref(), new TypeReference<Page<TenderTypeList>>() {});
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private Link<Page<DiscountList>> discounts;
        private Link<Page<TicketList>> tickets;
        private Link<Page<ClockEntryList>> clockEntries;
        private Link<Page<EmployeeList>> employees;
        private Link<Page<JobList>> jobs;
        private Link<Page<OrderTypeList>> orderTypes;
        private Link<Page<RevenueCenterList>> revenueCenters;
        private Link<Page<TableList>> tables;
        private Link<Page<TenderTypeList>> tenderTypes;

        private Builder() {
        }

        public Builder discounts(Link<Page<DiscountList>> discounts) {
            this.discounts = discounts;
            return this;
        }

        public Builder tickets(Link<Page<TicketList>> tickets) {
            this.tickets = tickets;
            return this;
        }

        @JsonProperty("clock_entries")
        public Builder clockEntries(Link<Page<ClockEntryList>> clockEntries) {
            this.clockEntries = clockEntries;
            return this;
        }

        public Builder employees(Link<Page<EmployeeList>> employees) {
            this.employees = employees;
            return this;
        }

        public Builder jobs(Link<Page<JobList>> jobs) {
            this.jobs = jobs;
            return this;
        }

        @JsonProperty("order_types")
        public Builder orderTypes(Link<Page<OrderTypeList>> orderTypes) {
            this.orderTypes = orderTypes;
            return this;
        }

        @JsonProperty("revenue_centers")
        public Builder revenueCenters(Link<Page<RevenueCenterList>> revenueCenters) {
            this.revenueCenters = revenueCenters;
            return this;
        }

        public Builder tables(Link<Page<TableList>> tables) {
            this.tables = tables;
            return this;
        }

        @JsonProperty("tender_types")
        public Builder tenderTypes(Link<Page<TenderTypeList>> tenderTypes) {
            this.tenderTypes = tenderTypes;
            return this;
        }

        public LocationLinks build() {
            return new LocationLinks(this);
        }
    }
}
