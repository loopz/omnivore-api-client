package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.OptionSet;
import com.loopz.omnivore.api.model.OrderType;
import com.loopz.omnivore.api.model.Page;
import java.util.List;

public class OptionSetList {

    private final List<OptionSet> optionSets;

    public OptionSetList(@JsonProperty("option_sets") List<OptionSet> optionSets) {
        this.optionSets = optionSets;
    }

    public List<OptionSet> getOrderTypes() {
        return optionSets;
    }

    public static Page<OptionSetList> list(String locationId, String ticketId) throws OmnivoreException {
        return OmnivoreApi.client().listOptionSets(locationId, ticketId);
    }

}
