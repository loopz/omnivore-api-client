package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = TicketTotals.Builder.class)
public class TicketTotals {

    private final int discounts;
    private final int due;
    private final int items;
    private final int otherCharges;
    private final int paid;
    private final int serviceCharges;
    private final int subTotal;
    private final int tax;
    private final int tips;
    private final int total;

    private TicketTotals(Builder builder) {
        discounts = builder.discounts;
        due = builder.due;
        items = builder.items;
        otherCharges = builder.otherCharges;
        paid = builder.paid;
        serviceCharges = builder.serviceCharges;
        subTotal = builder.subTotal;
        tax = builder.tax;
        tips = builder.tips;
        total = builder.total;
    }

    public static Builder builder() {
        return new Builder();
    }

    public int getDiscounts() {
        return discounts;
    }

    public int getDue() {
        return due;
    }

    public int getItems() {
        return items;
    }

    public int getOtherCharges() {
        return otherCharges;
    }

    public int getPaid() {
        return paid;
    }

    public int getServiceCharges() {
        return serviceCharges;
    }

    public int getSubTotal() {
        return subTotal;
    }

    public int getTax() {
        return tax;
    }

    public int getTips() {
        return tips;
    }

    public int getTotal() {
        return total;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private int discounts;
        private int due;
        private int items;
        private int otherCharges;
        private int paid;
        private int serviceCharges;
        private int subTotal;
        private int tax;
        private int tips;
        private int total;

        private Builder() {
        }

        public Builder discounts(int discounts) {
            this.discounts = discounts;
            return this;
        }

        public Builder due(int due) {
            this.due = due;
            return this;
        }

        public Builder items(int items) {
            this.items = items;
            return this;
        }

        @JsonProperty("other_charges")
        public Builder otherCharges(int otherCharges) {
            this.otherCharges = otherCharges;
            return this;
        }

        public Builder paid(int paid) {
            this.paid = paid;
            return this;
        }

        @JsonProperty("service_charges")
        public Builder serviceCharges(int serviceCharges) {
            this.serviceCharges = serviceCharges;
            return this;
        }

        @JsonProperty("sub_total")
        public Builder subTotal(int subTotal) {
            this.subTotal = subTotal;
            return this;
        }

        public Builder tax(int tax) {
            this.tax = tax;
            return this;
        }

        public Builder tips(int tips) {
            this.tips = tips;
            return this;
        }

        public Builder total(int total) {
            this.total = total;
            return this;
        }

        public TicketTotals build() {
            return new TicketTotals(this);
        }
    }
}
