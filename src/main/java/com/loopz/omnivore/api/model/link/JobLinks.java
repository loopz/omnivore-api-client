package com.loopz.omnivore.api.model.link;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Page;
import com.loopz.omnivore.api.model.list.ClockEntryList;

public class JobLinks {

    private final Link<Page<ClockEntryList>> clockEntries;

    public JobLinks(@JsonProperty("clock_entries") Link<Page<ClockEntryList>> clockEntries) {
        this.clockEntries = clockEntries;
    }

    public Page<ClockEntryList> getClockEntries() throws OmnivoreException {
        return OmnivoreApi.client().get(clockEntries.getHref(), new TypeReference<Page<ClockEntryList>>() {});

    }
}
