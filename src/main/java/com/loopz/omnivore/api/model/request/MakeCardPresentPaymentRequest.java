package com.loopz.omnivore.api.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MakeCardPresentPaymentRequest extends MakePaymentRequest{

    private final CardInfo cardInfo;

    protected MakeCardPresentPaymentRequest(Builder builder) {
        super(builder);
        cardInfo = builder.cardInfo;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String getType() {
        return "card_present";
    }

    @JsonProperty("card_info")
    public CardInfo getCardInfo() {
        return cardInfo;
    }

    public static final class Builder extends AbstractBuilder<MakeCardPresentPaymentRequest, Builder> {

        private CardInfo cardInfo;

        @Override
        public MakeCardPresentPaymentRequest build() {
            return new MakeCardPresentPaymentRequest(this);
        }

        public Builder cardInfo(CardInfo cardInfo) {
            this.cardInfo = cardInfo;
            return this;
        }
    }

    public static class CardInfo {

        private final String data;

        public CardInfo(String data) {
            this.data = data;
        }

        public String getData() {
            return data;
        }
    }
}
