package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Employee;
import com.loopz.omnivore.api.model.Page;
import java.util.List;

public class EmployeeList {

    private final List<Employee> employees;

    public EmployeeList(@JsonProperty("employees") List<Employee> employees) {
        this.employees = employees;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public static Page<EmployeeList> list(String locationId) throws OmnivoreException {
        return OmnivoreApi.client().listEmployees(locationId);
    }

}
