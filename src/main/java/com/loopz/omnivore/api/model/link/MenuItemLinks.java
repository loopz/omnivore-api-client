package com.loopz.omnivore.api.model.link;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Page;
import com.loopz.omnivore.api.model.list.CategoryList;
import com.loopz.omnivore.api.model.list.OptionSetList;
import com.loopz.omnivore.api.model.list.PriceLevelList;

@JsonDeserialize(builder = MenuItemLinks.Builder.class)
public class MenuItemLinks {

    private final Link<Page<CategoryList>> categories;
    private final Link<Page<OptionSetList>> optionSets;
    private final Link<Page<PriceLevelList>> priceLevels;

    private MenuItemLinks(Builder builder) {
        categories = builder.categories;
        optionSets = builder.optionSets;
        priceLevels = builder.priceLevels;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Page<CategoryList> getCategories() throws OmnivoreException {
        return OmnivoreApi.client().get(categories.getHref(), new TypeReference<Page<CategoryList>>() {});
    }

    public Page<OptionSetList> getOptionSets() throws OmnivoreException {
        return OmnivoreApi.client().get(categories.getHref(), new TypeReference<Page<OptionSetList>>() {});
    }

    public Page<PriceLevelList> getPriceLevels() throws OmnivoreException {
        return OmnivoreApi.client().get(categories.getHref(), new TypeReference<Page<PriceLevelList>>() {});
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private Link<Page<CategoryList>> categories;
        private Link<Page<OptionSetList>> optionSets;
        private Link<Page<PriceLevelList>> priceLevels;

        private Builder() {
        }

        public Builder categories(Link<Page<CategoryList>> categories) {
            this.categories = categories;
            return this;
        }

        @JsonProperty("option_sets")
        public Builder optionSets(Link<Page<OptionSetList>> optionSets) {
            this.optionSets = optionSets;
            return this;
        }

        @JsonProperty("price_levels")
        public Builder priceLevels(Link<Page<PriceLevelList>> priceLevels) {
            this.priceLevels = priceLevels;
            return this;
        }

        public MenuItemLinks build() {
            return new MenuItemLinks(this);
        }
    }
}
