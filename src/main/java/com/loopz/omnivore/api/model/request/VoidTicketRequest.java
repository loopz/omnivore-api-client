package com.loopz.omnivore.api.model.request;

public class VoidTicketRequest {

    private final boolean cancel;

    public VoidTicketRequest(boolean cancel) {
        this.cancel = cancel;
    }

    public boolean isVoid() {
        return cancel;
    }
}
