package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Page;
import com.loopz.omnivore.api.model.Table;
import java.util.List;

public class TableList {

    private final List<Table> tables;

    public TableList(@JsonProperty("tables") List<Table> tables) {
        this.tables = tables;
    }

    public List<Table> getTables() {
        return tables;
    }

    public static Page<TableList> list(String locationId) throws OmnivoreException {
        return OmnivoreApi.client().listTables(locationId);
    }

}
