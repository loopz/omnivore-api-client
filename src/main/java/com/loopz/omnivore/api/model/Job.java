package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.omnivore.api.model.link.JobLinks;

@JsonDeserialize(builder = Job.Builder.class)
public class Job {

    private final String id;
    private final String name;
    private final String posId;
    private final JobLinks links;

    private Job(Builder builder) {
        id = builder.id;
        name = builder.name;
        posId = builder.posId;
        links = builder.links;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPosId() {
        return posId;
    }

    public JobLinks getLinks() {
        return links;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private String posId;
        private JobLinks links;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        @JsonProperty("pos_id")
        public Builder posId(String posId) {
            this.posId = posId;
            return this;
        }

        @JsonProperty("_links")
        public Builder links(JobLinks links) {
            this.links = links;
            return this;
        }

        public Job build() {
            return new Job(this);
        }
    }
}
