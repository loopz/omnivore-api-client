package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.MenuItem;
import com.loopz.omnivore.api.model.Page;
import java.util.List;

public class MenuItemList {

    private final List<MenuItem> menuItems;

    public MenuItemList(@JsonProperty("menu_items") List<MenuItem> menuItems) {
        this.menuItems = menuItems;
    }

    public List<MenuItem> getMenuItems() {
        return menuItems;
    }

    public static Page<MenuItemList> list(String locationId) throws OmnivoreException {
        return OmnivoreApi.client().listMenuItems(locationId);
    }

}
