package com.loopz.omnivore.api.model.link;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Page;
import com.loopz.omnivore.api.model.TicketList;
import com.loopz.omnivore.api.model.list.ClockEntryList;

public class EmployeeLinks {

    private final Link<Page<ClockEntryList>> clockEntries;
    private final Link<Page<TicketList>> openTickets;

    public EmployeeLinks(
            @JsonProperty("clock_entries") Link<Page<ClockEntryList>> clockEntries,
            @JsonProperty("open_tickets") Link<Page<TicketList>> openTickets) {
        this.clockEntries = clockEntries;
        this.openTickets = openTickets;
    }

    public Page<ClockEntryList> getClockEntries() throws OmnivoreException {
        return OmnivoreApi.client().get(clockEntries.getHref(), new TypeReference<Page<ClockEntryList>>() {});
    }

    public Page<TicketList> getOpenTickets() throws OmnivoreException {
        return OmnivoreApi.client().get(openTickets.getHref(), new TypeReference<Page<TicketList>>() {});
    }
}
