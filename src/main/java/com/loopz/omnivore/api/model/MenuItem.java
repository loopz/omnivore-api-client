package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.omnivore.api.model.embedded.MenuItemEmbedded;
import com.loopz.omnivore.api.model.link.MenuItemLinks;
import java.util.List;

@JsonDeserialize(builder = MenuItem.Builder.class)
public class MenuItem {

    private final String id;
    private final String name;
    private final int pricePerUnit;
    private final String posId;
    private final boolean open;
    private final boolean inStock;
    private final MenuItemEmbedded embedded;
    private final MenuItemLinks links;

    private MenuItem(Builder builder) {
        id = builder.id;
        name = builder.name;
        pricePerUnit = builder.pricePerUnit;
        posId = builder.posId;
        open = builder.open;
        inStock = builder.inStock;
        embedded = builder.embedded;
        links = builder.links;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPricePerUnit() {
        return pricePerUnit;
    }

    public String getPosId() {
        return posId;
    }

    public boolean isOpen() {
        return open;
    }

    public boolean isInStock() {
        return inStock;
    }

    public MenuItemLinks getLinks() {
        return links;
    }

    public List<Category> getCategories() {
        return embedded.getCategories();
    }

    public List<OptionSet> getOptionSets() {
        return embedded.getOptionSets();
    }

    public List<PriceLevel> getPriceLevels() {
        return embedded.getPriceLevels();
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private int pricePerUnit;
        private String posId;
        private boolean open;
        private boolean inStock;
        private MenuItemEmbedded embedded;
        private MenuItemLinks links;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        @JsonProperty("price_per_unit")
        public Builder pricePerUnit(int pricePerUnit) {
            this.pricePerUnit = pricePerUnit;
            return this;
        }

        @JsonProperty("pos_id")
        public Builder posId(String posId) {
            this.posId = posId;
            return this;
        }

        public Builder open(boolean open) {
            this.open = open;
            return this;
        }

        @JsonProperty("in_stock")
        public Builder inStock(boolean inStock) {
            this.inStock = inStock;
            return this;
        }

        @JsonProperty("_embedded")
        public Builder embedded(MenuItemEmbedded embedded) {
            this.embedded = embedded;
            return this;
        }

        @JsonProperty("_links")
        public Builder links(MenuItemLinks links) {
            this.links = links;
            return this;
        }

        public MenuItem build() {
            return new MenuItem(this);
        }
    }
}
