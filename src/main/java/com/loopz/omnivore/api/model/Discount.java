package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = Discount.Builder.class)
public class Discount {

    private final int value;
    private final String type;
    private final String posId;
    private final boolean open;
    private final String name;
    private final int minTicketTotal;
    private final int minPercent;
    private final int minAmount;
    private final int maxPercent;
    private final int maxAmount;
    private final String id;
    private final boolean available;
    private final AppliesTo appliesTo;

    private Discount(Builder builder) {
        value = builder.value;
        type = builder.type;
        posId = builder.posId;
        open = builder.open;
        name = builder.name;
        minTicketTotal = builder.minTicketTotal;
        minPercent = builder.minPercent;
        minAmount = builder.minAmount;
        maxPercent = builder.maxPercent;
        maxAmount = builder.maxAmount;
        id = builder.id;
        available = builder.available;
        appliesTo = builder.appliesTo;
    }

    public static Builder builder() {
        return new Builder();
    }

    public int getValue() {
        return value;
    }

    public String getType() {
        return type;
    }

    public String getPosId() {
        return posId;
    }

    public boolean isOpen() {
        return open;
    }

    public String getName() {
        return name;
    }

    public int getMinTicketTotal() {
        return minTicketTotal;
    }

    public int getMinPercent() {
        return minPercent;
    }

    public int getMinAmount() {
        return minAmount;
    }

    public int getMaxPercent() {
        return maxPercent;
    }

    public int getMaxAmount() {
        return maxAmount;
    }

    public String getId() {
        return id;
    }

    public boolean isAvailable() {
        return available;
    }

    public AppliesTo getAppliesTo() {
        return appliesTo;
    }

    public static class AppliesTo {

        private final boolean ticket;
        private final boolean item;

        public AppliesTo(@JsonProperty("ticket") boolean ticket, @JsonProperty("item") boolean item) {
            this.ticket = ticket;
            this.item = item;
        }

        public boolean isTicket() {
            return ticket;
        }

        public boolean isItem() {
            return item;
        }
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private int value;
        private String type;
        private String posId;
        private boolean open;
        private String name;
        private int minTicketTotal;
        private int minPercent;
        private int minAmount;
        private int maxPercent;
        private int maxAmount;
        private String id;
        private boolean available;
        private AppliesTo appliesTo;

        private Builder() {
        }

        public Builder value(int value) {
            this.value = value;
            return this;
        }

        public Builder type(String type) {
            this.type = type;
            return this;
        }

        @JsonProperty("pos_id")
        public Builder posId(String posId) {
            this.posId = posId;
            return this;
        }

        public Builder open(boolean open) {
            this.open = open;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        @JsonProperty("min_ticket_total")
        public Builder minTicketTotal(int minTicketTotal) {
            this.minTicketTotal = minTicketTotal;
            return this;
        }

        @JsonProperty("min_percent")
        public Builder minPercent(int minPercent) {
            this.minPercent = minPercent;
            return this;
        }

        @JsonProperty("min_amount")
        public Builder minAmount(int minAmount) {
            this.minAmount = minAmount;
            return this;
        }

        @JsonProperty("max_percent")
        public Builder maxPercent(int maxPercent) {
            this.maxPercent = maxPercent;
            return this;
        }

        @JsonProperty("max_amount")
        public Builder maxAmount(int maxAmount) {
            this.maxAmount = maxAmount;
            return this;
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder available(boolean available) {
            this.available = available;
            return this;
        }

        @JsonProperty("applies_to")
        public Builder appliesTo(AppliesTo appliesTo) {
            this.appliesTo = appliesTo;
            return this;
        }

        public Discount build() {
            return new Discount(this);
        }
    }
}
