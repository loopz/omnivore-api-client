package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.embedded.TicketEmbedded;
import com.loopz.omnivore.api.model.link.TicketLinks;
import com.loopz.omnivore.api.model.request.AddTicketItemsRequest;
import com.loopz.omnivore.api.model.request.OpenTicketRequest;
import com.loopz.omnivore.api.model.request.VoidTicketRequest;
import java.time.Instant;
import java.util.List;

@JsonDeserialize(builder = Ticket.Builder.class)
public class Ticket {

    private final String id;
    private final String name;
    private final String ticketNumber;
    private final Instant openedAt;
    private final Instant closedAt;
    private final boolean voided;
    private final boolean open;
    private final int guestCount;
    private final boolean autoSend;
    private final TicketTotals totals;
    private final TicketEmbedded embedded;
    private final TicketLinks links;

    private Ticket(Builder builder) {
        id = builder.id;
        name = builder.name;
        ticketNumber = builder.ticketNumber;
        openedAt = builder.openedAt;
        closedAt = builder.closedAt;
        voided = builder.voided;
        open = builder.open;
        guestCount = builder.guestCount;
        autoSend = builder.autoSend;
        totals = builder.totals;
        embedded = builder.embedded;
        links = builder.links;
    }

    public static Ticket get(String locationId, String ticketId) throws OmnivoreException {
        return OmnivoreApi.client().getTicket(locationId, ticketId);
    }

    public static Ticket open(String locationId, OpenTicketRequest request) throws OmnivoreException {
        return OmnivoreApi.client().openTicket(locationId, request);
    }

    public static Ticket addItems(String locationId, String ticketId, AddTicketItemsRequest request) throws OmnivoreException {
        return OmnivoreApi.client().addTicketItems(locationId, ticketId, request);
    }

    public static Ticket voidItem(String locationId, String ticketId, String ticketItemId) throws OmnivoreException {
        return OmnivoreApi.client().voidTicketItem(locationId, ticketId, ticketItemId);
    }

    public static Ticket cancel(String locationId, String ticketId, boolean cancel) throws OmnivoreException {
        return OmnivoreApi.client().voidTicket(locationId, ticketId, new VoidTicketRequest(cancel));
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }

    public Instant getOpenedAt() {
        return openedAt;
    }

    public Instant getClosedAt() {
        return closedAt;
    }

    public boolean isVoid() {
        return voided;
    }

    public boolean isOpen() {
        return open;
    }

    public int getGuestCount() {
        return guestCount;
    }

    public boolean isAutoSend() {
        return autoSend;
    }

    public TicketTotals getTotals() {
        return totals;
    }

    public TicketLinks getLinks() {
        return links;
    }

    public List<TicketDiscount> getDiscounts() {
        return embedded.getDiscounts();
    }

    public List<Payment> getPayments() {
        return embedded.getPayments();
    }

    public Employee getEmployee() {
        return embedded.getEmployee();
    }

    public List<TicketItem> getItems() {
        return embedded.getItems();
    }

    public List<TicketItem> getVoidedItems() {
        return embedded.getVoidedItems();
    }

    public OrderType getOrderType() {
        return embedded.getOrderType();
    }

    public RevenueCenter getRevenueCenter() {
        return embedded.getRevenueCenter();
    }

    public Table getTable() {
        return embedded.getTable();
    }

    public List<ServiceCharge> getServiceCharges() {
        return embedded.getServiceCharges();
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private String ticketNumber;
        private Instant openedAt;
        private Instant closedAt;
        private boolean voided;
        private boolean open;
        private int guestCount;
        private boolean autoSend;
        private TicketTotals totals;
        private TicketEmbedded embedded;
        private TicketLinks links;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        @JsonProperty("ticket_number")
        public Builder ticketNumber(String ticketNumber) {
            this.ticketNumber = ticketNumber;
            return this;
        }

        @JsonProperty("opened_at")
        public Builder openedAt(Instant openedAt) {
            this.openedAt = openedAt;
            return this;
        }

        @JsonProperty("closed_at")
        public Builder closedAt(Instant closedAt) {
            this.closedAt = closedAt;
            return this;
        }

        @JsonProperty("void")
        public Builder voided(boolean voided) {
            this.voided = voided;
            return this;
        }

        public Builder open(boolean open) {
            this.open = open;
            return this;
        }

        @JsonProperty("guest_count")
        public Builder guestCount(int guestCount) {
            this.guestCount = guestCount;
            return this;
        }

        @JsonProperty("auto_send")
        public Builder autoSend(boolean autoSend) {
            this.autoSend = autoSend;
            return this;
        }

        public Builder totals(TicketTotals totals) {
            this.totals = totals;
            return this;
        }

        @JsonProperty("_embedded")
        public Builder embedded(TicketEmbedded embedded) {
            this.embedded = embedded;
            return this;
        }

        @JsonProperty("_links")
        public Builder links(TicketLinks links) {
            this.links = links;
            return this;
        }

        public Ticket build() {
            return new Ticket(this);
        }
    }
}
