package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.embedded.CategoryEmbedded;
import com.loopz.omnivore.api.model.link.CategoryLinks;
import com.loopz.omnivore.api.model.list.CategoryList;

@JsonDeserialize(builder = Category.Builder.class)
public class Category {

    private final String id;
    private final String name;
    private final int level;
    private final String posId;
    private final CategoryEmbedded embedded;
    private final CategoryLinks links;

    private Category(Builder builder) {
        id = builder.id;
        name = builder.name;
        level = builder.level;
        posId = builder.posId;
        embedded = builder.embedded;
        links = builder.links;
    }

    public static Builder builder() {
        return new Builder();
    }

    public static Category get(String locationId, String categoryId) throws OmnivoreException {
        return OmnivoreApi.client().getCategory(locationId, categoryId);
    }

    public static Page<CategoryList> get(String locationId) throws OmnivoreException {
        return OmnivoreApi.client().listCategories(locationId);
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getLevel() {
        return level;
    }

    public String getPosId() {
        return posId;
    }

    public CategoryLinks getLinks() {
        return links;
    }

    public Category getParentCategory() {
        return embedded.getParentCategory();
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private int level;
        private String posId;
        private CategoryEmbedded embedded;
        private CategoryLinks links;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder level(int level) {
            this.level = level;
            return this;
        }

        @JsonProperty("pos_id")
        public Builder posId(String posId) {
            this.posId = posId;
            return this;
        }

        public Builder embedded(CategoryEmbedded embedded) {
            this.embedded = embedded;
            return this;
        }

        public Builder links(CategoryLinks links) {
            this.links = links;
            return this;
        }

        public Category build() {
            return new Category(this);
        }
    }
}
