package com.loopz.omnivore.api.model.embedded;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.model.Employee;
import com.loopz.omnivore.api.model.Job;

public class ClockEntryEmbedded {

    private final Employee employee;
    private final Job job;

    public ClockEntryEmbedded(
            @JsonProperty("employee") Employee employee,
            @JsonProperty("job") Job job) {
        this.employee = employee;
        this.job = job;
    }

    public Employee getEmployee() {
        return employee;
    }

    public Job getJob() {
        return job;
    }
}
