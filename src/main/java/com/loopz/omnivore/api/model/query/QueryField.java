package com.loopz.omnivore.api.model.query;

public enum QueryField {

    ID("id", QueryFieldType.STRING),
    OPEN("open", QueryFieldType.BOOLEAN),
    OPENED_AT("opened_at", QueryFieldType.TIMESTAMP),
    AUTO_SEND("auto_send", QueryFieldType.BOOLEAN),
    CLOSED_AT("closed_at", QueryFieldType.TIMESTAMP),
    TICKET_NUMBER("ticket_number", QueryFieldType.NUMBER),
    ORDER_TYPE("@order_type.id", QueryFieldType.STRING),
    REVENUE_CENTER_ID("@revenue_center.id", QueryFieldType.STRING),
    EMPLOYEE_ID("@employee.id", QueryFieldType.STRING),
    TABLE_ID("@table.id", QueryFieldType.STRING),
    HEALTH_HEALTHY("health.healthy", QueryFieldType.BOOLEAN),
    STATUS("status", QueryFieldType.STRING),
    ;

    private final String name;
    private final QueryFieldType type;

    QueryField(String name, QueryFieldType type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public QueryFieldType getType() {
        return type;
    }
}
