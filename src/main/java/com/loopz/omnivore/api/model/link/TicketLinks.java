package com.loopz.omnivore.api.model.link;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.*;
import com.loopz.omnivore.api.model.list.PaymentList;
import com.loopz.omnivore.api.model.list.ServiceChargeList;
import com.loopz.omnivore.api.model.list.TicketItemList;

@JsonDeserialize(builder = TicketLinks.Builder.class)
public class TicketLinks {

    private final Link<Page<TicketDiscount>> discounts;
    private final Link<Page<TicketItemList>> items;
    private final Link<Page<PaymentList>> payments;
    private final Link<Page<ServiceChargeList>> serviceCharges;
    private final Link<Page<TicketItemList>> voidedItems;
    private final Link<Employee> employee;
    private final Link<OrderType> orderType;
    private final Link<RevenueCenter> revenueCenter;
    private final Link<Table> table;

    private TicketLinks(Builder builder) {
        discounts = builder.discounts;
        items = builder.items;
        payments = builder.payments;
        serviceCharges = builder.serviceCharges;
        voidedItems = builder.voidedItems;
        employee = builder.employee;
        orderType = builder.orderType;
        revenueCenter = builder.revenueCenter;
        table = builder.table;
    }

    public static Builder builder() {
        return new Builder();
    }

    public Page<TicketDiscount> getDiscounts() throws OmnivoreException {
        return OmnivoreApi.client().get(discounts.getHref(), new TypeReference<Page<TicketDiscount>>() {});
    }

    public Page<TicketItemList> getItems() throws OmnivoreException {
        return OmnivoreApi.client().get(items.getHref(), new TypeReference<Page<TicketItemList>>() {});
    }

    public Page<PaymentList> getPayments() throws OmnivoreException {
        return OmnivoreApi.client().get(payments.getHref(), new TypeReference<Page<PaymentList>>() {});
    }

    public Page<ServiceChargeList> getServiceCharges() throws OmnivoreException {
        return OmnivoreApi.client().get(serviceCharges.getHref(), new TypeReference<Page<ServiceChargeList>>() {});
    }

    public Page<TicketItemList> getVoidedItems() throws OmnivoreException {
        return OmnivoreApi.client().get(voidedItems.getHref(), new TypeReference<Page<TicketItemList>>() {});
    }

    public Page<Employee> getEmployee() throws OmnivoreException {
        return OmnivoreApi.client().get(employee.getHref(), new TypeReference<Page<Employee>>() {});
    }

    public Page<OrderType> getOrderType() throws OmnivoreException {
        return OmnivoreApi.client().get(orderType.getHref(), new TypeReference<Page<OrderType>>() {});
    }

    public Page<RevenueCenter> getRevenueCenter() throws OmnivoreException {
        return OmnivoreApi.client().get(revenueCenter.getHref(), new TypeReference<Page<RevenueCenter>>() {});
    }

    public Page<Table> getTable() throws OmnivoreException {
        return OmnivoreApi.client().get(table.getHref(), new TypeReference<Page<Table>>() {});
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private Link<Page<TicketDiscount>> discounts;
        private Link<Page<TicketItemList>> items;
        private Link<Page<PaymentList>> payments;
        private Link<Page<ServiceChargeList>> serviceCharges;
        private Link<Page<TicketItemList>> voidedItems;
        private Link<Employee> employee;
        private Link<OrderType> orderType;
        private Link<RevenueCenter> revenueCenter;
        private Link<Table> table;

        private Builder() {
        }

        public Builder discounts(Link<Page<TicketDiscount>> discounts) {
            this.discounts = discounts;
            return this;
        }

        public Builder items(Link<Page<TicketItemList>> items) {
            this.items = items;
            return this;
        }

        public Builder payments(Link<Page<PaymentList>> payments) {
            this.payments = payments;
            return this;
        }

        @JsonProperty("service_charges")
        public Builder serviceCharges(Link<Page<ServiceChargeList>> serviceCharges) {
            this.serviceCharges = serviceCharges;
            return this;
        }

        @JsonProperty("voided_items")
        public Builder voidedItems(Link<Page<TicketItemList>> voidedItems) {
            this.voidedItems = voidedItems;
            return this;
        }

        public Builder employee(Link<Employee> employee) {
            this.employee = employee;
            return this;
        }

        @JsonProperty("order_type")
        public Builder orderType(Link<OrderType> orderType) {
            this.orderType = orderType;
            return this;
        }

        @JsonProperty("revenue_center")
        public Builder revenueCenter(Link<RevenueCenter> revenueCenter) {
            this.revenueCenter = revenueCenter;
            return this;
        }

        public Builder table(Link<Table> table) {
            this.table = table;
            return this;
        }

        public TicketLinks build() {
            return new TicketLinks(this);
        }
    }
}
