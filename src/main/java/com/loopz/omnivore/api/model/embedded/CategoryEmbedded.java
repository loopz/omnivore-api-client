package com.loopz.omnivore.api.model.embedded;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.model.Category;

public class CategoryEmbedded {

    private final Category parentCategory;

    public CategoryEmbedded(
            @JsonProperty("parent_category") Category parentCategory) {
        this.parentCategory = parentCategory;
    }

    public Category getParentCategory() {
        return parentCategory;
    }
}
