package com.loopz.omnivore.api.model.list;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.loopz.omnivore.api.OmnivoreApi;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.Page;
import com.loopz.omnivore.api.model.TicketItem;
import java.util.List;

public class TicketItemList {

    private final List<TicketItem> items;

    public TicketItemList(@JsonProperty("items") List<TicketItem> items) {
        this.items = items;
    }

    public List<TicketItem> getItems() {
        return items;
    }

    public static Page<TicketItemList> list(String locationId, String ticketId) throws OmnivoreException {
        return OmnivoreApi.client().listTicketItems(locationId, ticketId);
    }

}
