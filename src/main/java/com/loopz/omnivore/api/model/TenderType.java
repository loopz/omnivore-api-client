package com.loopz.omnivore.api.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;

@JsonDeserialize(builder = TenderType.Builder.class)
public class TenderType {

    private final String id;
    private final String name;
    private final String posId;
    private final boolean allowsTips;

    private TenderType(Builder builder) {
        id = builder.id;
        name = builder.name;
        posId = builder.posId;
        allowsTips = builder.allowsTips;
    }

    public static Builder builder() {
        return new Builder();
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPosId() {
        return posId;
    }

    public boolean isAllowsTips() {
        return allowsTips;
    }

    @JsonPOJOBuilder(withPrefix = "")
    public static final class Builder {
        private String id;
        private String name;
        private String posId;
        private boolean allowsTips;

        private Builder() {
        }

        public Builder id(String id) {
            this.id = id;
            return this;
        }

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        @JsonProperty("pos_id")
        public Builder posId(String posId) {
            this.posId = posId;
            return this;
        }

        @JsonProperty("allows_tips")
        public Builder allowsTips(boolean allowsTips) {
            this.allowsTips = allowsTips;
            return this;
        }

        public TenderType build() {
            return new TenderType(this);
        }
    }
}
