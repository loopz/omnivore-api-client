package com.loopz.omnivore.api;

public class OmnivoreException extends Exception {

    private int statusCode;
    private OmnivoreErrorResponse errorResponse;

    public OmnivoreException(int statusCode, OmnivoreErrorResponse errorResponse) {
        super();
        this.statusCode = statusCode;
        this.errorResponse = errorResponse;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public OmnivoreErrorResponse getErrorResponse() {
        return errorResponse;
    }

    @Override
    public String toString() {
        return "OmnivoreException [" +
                "statusCode=" + statusCode +
                ", errors=" + errorResponse.getErrors() +
                ']';
    }
}
