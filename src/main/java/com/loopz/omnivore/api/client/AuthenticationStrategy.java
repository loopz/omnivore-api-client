package com.loopz.omnivore.api.client;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public interface AuthenticationStrategy {

    void applyCredentials(OkHttpClient.Builder clientBuilder, Request.Builder requestBuilder);

    String getAuthenticatingUserName();
}
