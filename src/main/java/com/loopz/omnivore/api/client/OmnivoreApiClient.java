package com.loopz.omnivore.api.client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.model.*;
import com.loopz.omnivore.api.model.list.*;
import com.loopz.omnivore.api.model.query.Query;
import com.loopz.omnivore.api.model.query.QueryBuilder;
import com.loopz.omnivore.api.model.request.AddTicketItemsRequest;
import com.loopz.omnivore.api.model.request.MakePaymentRequest;
import com.loopz.omnivore.api.model.request.OpenTicketRequest;
import com.loopz.omnivore.api.model.request.VoidTicketRequest;
import okhttp3.Request;

public class OmnivoreApiClient extends BaseApiClient {

    public OmnivoreApiClient(String rootPath, AuthenticationStrategy authStrategy) {
        super(rootPath, authStrategy);
    }

    public <T> T get(String url, TypeReference<T> typeReference) throws OmnivoreException {
        Request.Builder get = createHttpGet(url);
        return execute(get, typeReference);
    }

    public void delete(String url) throws OmnivoreException {
        Request.Builder delete = createHttpDelete(url);
        execute(delete);
    }

    public Page<LocationList> listLocations() throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations");
        return execute(get, new TypeReference<Page<LocationList>>() {});
    }

    public Location getLocation(String locationId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}", locationId);
        return execute(get, Location.class);
    }

    public Page<TicketList> listTickets(String locationId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/tickets", locationId);
        return execute(get, new TypeReference<Page<TicketList>>() {});
    }

    public Page<TicketList> listTickets(String locationId, Query query) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/tickets?" + query.toString(), locationId);
        return execute(get, new TypeReference<Page<TicketList>>() {});
    }

    public Ticket getTicket(String locationId, String ticketId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/tickets/{1}", locationId, ticketId);
        return execute(get, Ticket.class);
    }

    public Ticket openTicket(String locationId, OpenTicketRequest request) throws OmnivoreException {
        Request.Builder post = createHttpPost(rootPath + "/locations/{0}/tickets", locationId)
                .body(request);
        return execute(post, Ticket.class);
    }

    public Ticket voidTicket(String locationId, String ticketId, VoidTicketRequest request) throws OmnivoreException {
        Request.Builder post = createHttpPost(rootPath + "/locations/{0}/tickets/{1}", locationId, ticketId)
                .body(request);
        return execute(post, Ticket.class);
    }

    public Page<TicketItemList> listTicketItems(String locationId, String ticketId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/tickets/{1}/items", locationId, ticketId);
        return execute(get,  new TypeReference<Page<TicketItemList>>() {});
    }

    public TicketItem getTicketItem(String locationId, String ticketId, String ticketItemId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/tickets/{1}/items/{2}", locationId, ticketId, ticketItemId);
        return execute(get, TicketItem.class);
    }

    public Page<PaymentList> listPayments(String locationId, String ticketId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/tickets/{1}/payments", locationId, ticketId);
        return execute(get,  new TypeReference<Page<PaymentList>>() {});
    }

    public Page<TicketDiscountList> listTicketDiscounts(String locationId, String ticketId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/tickets/{1}/discounts", locationId, ticketId);
        return execute(get,  new TypeReference<Page<TicketDiscountList>>() {});
    }

    public Page<TableList> listTables(String locationId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/tables", locationId);
        return execute(get,  new TypeReference<Page<TableList>>() {});
    }

    public Page<ServiceChargeList> listServiceCharges(String locationId, String ticketId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/tickets/{1}/service_charges", locationId, ticketId);
        return execute(get,  new TypeReference<Page<ServiceChargeList>>() {});
    }

    public Page<TenderTypeList> listTenderTypes(String locationId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/tender_types", locationId);
        return execute(get, new TypeReference<Page<TenderTypeList>>() {});
    }

    public Page<EmployeeList> listEmployees(String locationId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/employees", locationId);
        return execute(get, new TypeReference<Page<EmployeeList>>() {});
    }

    public Page<JobList> listJobs(String locationId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/jobs", locationId);
        return execute(get, new TypeReference<Page<JobList>>() {});
    }

    public Page<OrderTypeList> listOrderTypes(String locationId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/order_types", locationId);
        return execute(get, new TypeReference<Page<OrderTypeList>>() {});
    }

    public Page<RevenueCenterList> listRevenueCenters(String locationId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/revenue_centers", locationId);
        return execute(get, new TypeReference<Page<RevenueCenterList>>() {});
    }

    public Category getCategory(String locationId, String categoryId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/menu/categories/{1}", locationId, categoryId);
        return execute(get, Category.class);
    }

    public Page<CategoryList> listCategories(String locationId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/menu/categories", locationId);
        return execute(get, new TypeReference<Page<CategoryList>>() {});
    }

    public Page<OptionSetList> listOptionSets(String locationId, String menuItemId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/menu/items/{1}/option_sets", locationId, menuItemId);
        return execute(get, new TypeReference<Page<OptionSetList>>() {});
    }

    public Page<PriceLevelList> listPriceLevels(String locationId, String menuItemId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/menu/items/{1}/price_levels", locationId, menuItemId);
        return execute(get, new TypeReference<Page<PriceLevelList>>() {});
    }

    public Page<MenuItemList> listMenuItems(String locationId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/menu/items", locationId);
        return execute(get, new TypeReference<Page<MenuItemList>>() {});
    }

    public Payment getPayment(String locationId, String ticketId, String paymentId) throws OmnivoreException {
        Request.Builder get = createHttpGet(rootPath + "/locations/{0}/tickets/{1}/payments/{2}", locationId, ticketId, paymentId);
        return execute(get, Payment.class);
    }

    public Payment makePayment(String locationId, String ticketId, MakePaymentRequest paymentRequest) throws OmnivoreException {
        Request.Builder post = createHttpPost(rootPath + "/locations/{0}/tickets/{1}/payments", locationId, ticketId)
                .body(paymentRequest);
        return execute(post, Payment.class);
    }

    public Ticket addTicketItems(String locationId, String ticketId, AddTicketItemsRequest request) throws OmnivoreException {
        Request.Builder post = createHttpPost(rootPath + "/locations/{0}/tickets/{1}/items", locationId, ticketId)
                .body(request);
        return execute(post, Ticket.class);
    }

    public Ticket voidTicketItem(String locationId, String ticketId, String ticketItemId) throws OmnivoreException {
        Request.Builder delete = createHttpDelete(rootPath + "/locations/{0}/tickets/{1}/items/{2}", locationId, ticketId, ticketItemId);
        return execute(delete, Ticket.class);
    }
}
