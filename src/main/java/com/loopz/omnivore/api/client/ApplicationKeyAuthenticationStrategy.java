package com.loopz.omnivore.api.client;

import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Applies the Omnivore Application Key to the given request.
 */
public class ApplicationKeyAuthenticationStrategy implements AuthenticationStrategy {

    private static final String HEADER = "Api-Key";

    private final String apiKey;

    public ApplicationKeyAuthenticationStrategy(String apiKey) {
        this.apiKey = apiKey;
    }

    @Override
    public void applyCredentials(OkHttpClient.Builder clientBuilder, Request.Builder requestBuilder) {
        requestBuilder.addHeader(HEADER, apiKey);
    }

    @Override
    public String getAuthenticatingUserName() {
        return "api-key";
    }
}
