package com.loopz.omnivore.api.client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.loopz.omnivore.api.OmnivoreErrorResponse;
import com.loopz.omnivore.api.OmnivoreException;
import com.loopz.omnivore.api.util.JsonUtils;
import okhttp3.*;
import okhttp3.logging.HttpLoggingInterceptor;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.io.IOException;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.text.MessageFormat;

import static com.loopz.omnivore.api.util.JsonUtils.fromJSON;
import static com.loopz.omnivore.api.util.JsonUtils.toJSON;

public abstract class BaseApiClient {

    /*
     * This is very bad practice and should NOT be used in production.
     */
    private static final TrustManager[] trustAllCerts = new TrustManager[] {
            new X509TrustManager() {
                @Override
                public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[]{};
                }
            }
    };

    private static final SSLContext trustAllSslContext;
    static {
        try {
            trustAllSslContext = SSLContext.getInstance("SSL");
            trustAllSslContext.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            throw new RuntimeException(e);
        }
    }
    private static final SSLSocketFactory trustAllSslSocketFactory = trustAllSslContext.getSocketFactory();

    private LogWrapper logWrapper;

    private static final int STATUS_OK = 200;
    private static final int STATUS_CREATED = 201;
    private static final int STATUS_NO_CONTENT = 204;

    private static final MediaType APPLICATION_JSON = MediaType.get("application/json");
    private static final MediaType APPLICATION_HAL_JSON = MediaType.get("application/hal+json");
    private static final MediaType TEXT_PLAIN = MediaType.get("text/plain");
    private static final MediaType TEXT_HTML = MediaType.get("text/html");

    protected String rootPath;
    private AuthenticationStrategy authStrategy;
    private ObjectMapper objectMapper;

    public BaseApiClient(String rootPath, AuthenticationStrategy authStrategy) {
        if (rootPath == null) {
            throw new IllegalArgumentException("rootPath cannot be null");
        }
        setAuthStrategy(authStrategy);
        this.rootPath = rootPath;
        this.authStrategy = authStrategy;
        setLogger(new SLF4JWrapper(getClass().getSimpleName()));
    }

    public AuthenticationStrategy getAuthStrategy() {
        return authStrategy;
    }

    public void setAuthStrategy(AuthenticationStrategy authStrategy) {
        if (authStrategy == null) {
            throw new IllegalArgumentException("AuthenticationStrategy cannot be null");
        }
        this.authStrategy = authStrategy;
    }

    public void execute(Request.Builder requestBuilder) throws OmnivoreException {
        executeAndParseResponse(requestBuilder);
    }

    public <T> T execute(Request.Builder requestBuilder, Class<T> targetType) throws OmnivoreException {
        JsonNode jsonNode = executeAndParseResponse(requestBuilder);
        return JsonUtils.fromJSON(jsonNode, targetType);
    }

    public <T> T execute(Request.Builder requestBuilder, TypeReference<T> targetTypeReference) throws OmnivoreException {
        JsonNode jsonNode = executeAndParseResponse(requestBuilder);
        return JsonUtils.fromJSON(jsonNode, targetTypeReference);
    }

    private JsonNode executeAndParseResponse(Request.Builder requestBuilder) throws OmnivoreException {
        JsonNode responseObject = null;

        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder()
                .addInterceptor(getLoggingInterceptor())
                .retryOnConnectionFailure(false);

        clientBuilder.sslSocketFactory(trustAllSslSocketFactory, (X509TrustManager)trustAllCerts[0]);
        clientBuilder.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        });

        authStrategy.applyCredentials(clientBuilder, requestBuilder);

        Response response = null;
        OkHttpClient httpClient = clientBuilder.build();
        try {
            response = httpClient.newCall(requestBuilder.build()).execute();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        int statusCode = response.code();
        switch (statusCode) {
            case STATUS_OK:
            case STATUS_CREATED:
                responseObject = readResponse(response, JsonNode.class);
                break;
            case STATUS_NO_CONTENT:
                break;
            default:
                throw new OmnivoreException(statusCode, readErrorResponse(response));
        }
        return responseObject;

    }


    protected HttpLoggingInterceptor getLoggingInterceptor() {
        HttpLoggingInterceptor logger = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String s) {
                logWrapper.debug(s);
            }
        });
        logger.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        return logger;
    }

    private OmnivoreErrorResponse readErrorResponse(Response response) {
        try {
            String content = response.body().string();
            return fromJSON(content, OmnivoreErrorResponse.class);
        } catch (Exception e) {
            return OmnivoreErrorResponse.unknown();
        }
    }

    private <T> T readResponse(Response response, Class<T> targetType) throws OmnivoreException {
        T responseObject = null;
        try {
            String content = response.body().string();
            if (targetType != null) {
                if (isContentTypeJson(response)) {
                    responseObject = fromJSON(content, targetType);
                }
            }
        } catch (IOException e) {
            throw new OmnivoreException(STATUS_OK, null);
        }
        return responseObject;
    }

    private boolean isContentTypeJson(Response response) {
        String contentTypeHeader = response.header("Content-Type");
        return contentTypeHeader.contains(APPLICATION_JSON.toString()) || contentTypeHeader.contains(APPLICATION_HAL_JSON.toString()) ;
    }

    private boolean isContentTypeText(Response response) {
        String contentTypeHeader = response.header("Content-Type");
        return contentTypeHeader.contains(TEXT_PLAIN.toString()) || contentTypeHeader.contains(TEXT_HTML.toString());
    }

    protected Request.Builder createHttpGet(String tokenizedUrl, Object... params) {
        return new Request.Builder().url(encodeParameters(tokenizedUrl, params)).get();
    }

    protected <T> PutRequestWithBody createHttpPut(String tokenizedUrl, Object... params) {
        return new PutRequestWithBody(encodeParameters(tokenizedUrl, params));
    }

    protected Request.Builder createHttpDelete(String tokenizedUrl, Object... params) {
        return new Request.Builder().url(encodeParameters(tokenizedUrl, params)).delete();
    }

    protected <T> PostRequestWithBody createHttpPost(String tokenizedUrl, Object... params) {
        return new PostRequestWithBody(encodeParameters(tokenizedUrl, params));
    }

    private String encodeParameters(String tokenizedUrl, Object... params) {
        Object[] encodedParams = new Object[params.length];
        int len = params.length;
        for (int i = 0; i < len; i++) {
            try {
                encodedParams[i] = URLEncoder.encode(String.valueOf(params[i]), "UTF-8");
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        return MessageFormat.format(tokenizedUrl, encodedParams);
    }


    public void setLogger(LogWrapper logWrapper) {
        this.logWrapper = logWrapper;
    }

    public interface LogWrapper {

        public void info(String message);

        public void debug(String message);

        public void debug(String message, Throwable e);

        public void error(String message, Throwable e);

    }

    public abstract class RequestWithBody {

        protected final Request.Builder requestBuilder;

        public RequestWithBody(String url) {
            requestBuilder = new Request.Builder()
                    .url(url);
        }

        public abstract <T> Request.Builder body(T content);

        public abstract Request.Builder empty();

    }

    public class PutRequestWithBody extends RequestWithBody {

        public PutRequestWithBody(String url) {
            super(url);
        }

        @Override
        public <T> Request.Builder body(T content) {
            return requestBuilder.put(RequestBody.create(APPLICATION_JSON, toJSON(content)));
        }

        @Override
        public Request.Builder empty() {
            return requestBuilder.put(RequestBody.create(null, new byte[]{}));
        }
    }

    public class PostRequestWithBody extends RequestWithBody {

        public PostRequestWithBody(String url) {
            super(url);
        }

        @Override
        public <T> Request.Builder body(T content) {
            return requestBuilder.post(RequestBody.create(APPLICATION_JSON, toJSON(content)));
        }

        @Override
        public Request.Builder empty() {
            return requestBuilder.post(RequestBody.create(null, new byte[]{}));
        }
    }

    public class SLF4JWrapper implements LogWrapper {

        private String tag;
        private Logger log;

        public SLF4JWrapper(String tag) {
            this.tag = tag;
        }

        private Logger getLog() {
            if (log == null) {
                log = LoggerFactory.getLogger(tag);
            }
            return log;
        }

        @Override
        public void info(String message) {
            getLog().info(message);
        }

        @Override
        public void debug(String message) {
            getLog().debug(message);
        }

        @Override
        public void debug(String message, Throwable e) {
            getLog().debug(message, e);
        }

        @Override
        public void error(String message, Throwable e) {
            getLog().error(message, e);
        }
    }
}
