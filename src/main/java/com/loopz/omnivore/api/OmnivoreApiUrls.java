package com.loopz.omnivore.api;

public class OmnivoreApiUrls {

    public static final String V_0_1 = "https://api.omnivore.io/0.1/";
    public static final String V_1_0 = "https://api.omnivore.io/1.0";

    private OmnivoreApiUrls(){}
}
