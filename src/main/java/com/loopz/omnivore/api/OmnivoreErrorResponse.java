package com.loopz.omnivore.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Arrays;
import java.util.List;

public class OmnivoreErrorResponse {

	private final List<OmnivoreError> errors;

	public OmnivoreErrorResponse(@JsonProperty("errors") List<OmnivoreError> errors) {
		this.errors = errors;
	}

	public static OmnivoreErrorResponse unknown() {
		OmnivoreError error = OmnivoreError.builder()
				.error("unkown_error")
				.description("An unknown error occurred.")
				.build();
		return new OmnivoreErrorResponse(Arrays.asList(error));
	}

	public List<OmnivoreError> getErrors() {
		return errors;
	}
}
